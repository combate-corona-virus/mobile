import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:mobile_covid/keys.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/localizacao-municipio-page.dart';
import 'package:mobile_covid/utils/consts/custom-colors.dart';
import 'package:url_launcher/url_launcher.dart';

class CidadeNaoParticipantePage extends StatelessWidget {
  static String tag = 'cidade-nao-participante-page';

  _launchURL() async {
    const url = 'mailto:combatecoronavirus@institutosoma.org.br';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  Row _trocarMunicipioButton() => Row(
    mainAxisAlignment: MainAxisAlignment.end,
    children: <Widget>[
      InkWell(
        onTap: () => (Keys.navKey.currentState.canPop()) ? Keys.navKey.currentState.pop() : Keys.navKey.currentState.pushReplacementNamed(LocalizacaoMunicipioPage.tag),
        child: Text(
          'TROCAR DE MUNICÍPIO',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
            color: CustomColors.customDarkGreen,
          ),
        ),
      ),
    ],
  );

  Container _titleGreenBar(BuildContext context) => Container(
    width: MediaQuery.of(context).size.width - 16,
    padding: EdgeInsets.only(left: 16),
    height: 40,
    decoration: BoxDecoration(
      color: CustomColors.customDarkGreen,
    ),
    alignment: Alignment.centerLeft,
    child: Row(
      children: <Widget>[
        Expanded(
          child: AutoSizeText(
            'O que devo fazer?',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 24,
            ),
          ),
        )
      ],
    ),
  );

  Column _cityNotAvailableMessage(String _cityName, BuildContext context) => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text.rich(
          TextSpan(
            style: TextStyle(
              fontSize: 18,
            ),
            children: <TextSpan>[
              TextSpan(text: _cityName, style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(text: ' ainda não faz parte da plataforma Combate Coronavírus.'),
            ],
          ),
          textAlign: TextAlign.center,
        ),
      ),
      SizedBox(height: 24),
      _titleGreenBar(context),
      SizedBox(height: 8),
      Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text.rich(
          TextSpan(
            style: TextStyle(
              fontSize: 14,
            ),
            children: <TextSpan>[
              TextSpan(text: 'Entre em contato com a sua Prefeitura Municipal e peça para que o responsável envie um e-mail para '),
              TextSpan(
                text: 'combatecoronavirus@institutosoma.org.br',
                style: TextStyle(
                  color: Colors.lightBlue,
                  decoration: TextDecoration.underline,
                ),
                recognizer: new TapGestureRecognizer()
                  ..onTap = _launchURL,
              ),
              TextSpan(text: ' solicitando acesso.'),
            ],
          ),
          textAlign: TextAlign.start,
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text.rich(
          TextSpan(
            style: TextStyle(
              fontSize: 14,
            ),
            children: <TextSpan>[
              TextSpan(text: 'A plataforma Combate Coronavírus é totalmente gratuita e foi desenvolvida de forma voluntária, tendo como missão oferecer ferramentas de inteligência espacial para apoio aos municípios brasileiros no combate à pandemia de COVID-19.'),
            ],
          ),
          textAlign: TextAlign.start,
        ),
      ),
    ],
  );

  @override
  Widget build(BuildContext context) {
    final String _cityName = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraint) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(minHeight: constraint.maxHeight),
              child: IntrinsicHeight(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 16),
                            Image.asset(
                              'assets/images/logo.png',
                              height: 322,
                            ),
                            _cityNotAvailableMessage(_cityName, context),
                            SizedBox(height: 32),
                            Expanded(child: Container()),
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: _trocarMunicipioButton(),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
              ),
            ),
          );
        },
      )
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/pages/initial-setup-widget/+model/initial-setup-widget-model.dart';
import 'package:mobile_covid/pages/initial-setup-widget/+state/initial-setup-widget-actions.dart';

class InitialSetupWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, InitialSetupWidgetModel>(
      distinct: true,
      converter: (store) => InitialSetupWidgetModel.fromStore(store),
      onInit: (store) => store.dispatch(new InitializeHive()),
      builder: (BuildContext context,InitialSetupWidgetModel initialSetupWidgetModel) {
        return Container();
      },
    );
  }

}

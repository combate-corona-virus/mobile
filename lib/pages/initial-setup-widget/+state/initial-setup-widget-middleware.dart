

import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:mobile_covid/keys.dart';
import 'package:mobile_covid/pages/home-page/home-page.dart';
import 'package:mobile_covid/pages/initial-setup-widget/+state/initial-setup-widget-actions.dart';
import 'package:mobile_covid/pages/introduction-page/introduction-page.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/+state/localizacao-municipio-page-actions.dart';
import 'package:redux/redux.dart';

class InitialSetupWidgetMiddleware extends MiddlewareClass {

  @override
  void call(Store store, action, NextDispatcher next) async {

    if (action is VerifyPreferences) {
      final Box municipioBox  = await Hive.openBox('municipio');
      final Map<String, dynamic> municipio = municipioBox.isNotEmpty ? Map.from(municipioBox.getAt(0)) : null;
      if(municipio != null) {
        store.dispatch(SelectMunicipio(municipio));
        Keys.navKey.currentState.pushReplacementNamed(HomePage.tag);
      } else {
          Keys.navKey.currentState.pushReplacementNamed(IntroductionPage.tag);
      }
    }

    if (action is DeletePreferences) {
      final Box municipioBox  = await Hive.openBox('municipio');
      await municipioBox.clear();
    }

    if (action is SavePreferences) {
      final Box municipioBox  = await Hive.openBox('municipio');
      municipioBox.add(action.municipio);
    }

    if (action is InitializeHive) {
      await Hive.initFlutter();
      store.dispatch(new VerifyPreferences());
    }

    next(action);

  }
}
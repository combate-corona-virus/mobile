import 'package:meta/meta.dart';

@immutable
class InitialSetupWidgetState{

  InitialSetupWidgetState();

  factory InitialSetupWidgetState.initial(){
    return new InitialSetupWidgetState();
  }

  InitialSetupWidgetState copyWith(){
    return new InitialSetupWidgetState();
  }
}

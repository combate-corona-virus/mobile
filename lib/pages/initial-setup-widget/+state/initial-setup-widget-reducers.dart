import 'package:mobile_covid/pages/initial-setup-widget/+state/initial-setup-widget-actions.dart';
import 'package:mobile_covid/pages/initial-setup-widget/+state/initial-setup-widget-state.dart';
import 'package:redux/redux.dart';

final initialSetupWidgetReducers = combineReducers<InitialSetupWidgetState>([
  TypedReducer<InitialSetupWidgetState,VerifyPreferences>(_verifyPreferences),
  TypedReducer<InitialSetupWidgetState,DeletePreferences>(_deletePreferences),
  TypedReducer<InitialSetupWidgetState,SavePreferences>(_savePreferences),
  TypedReducer<InitialSetupWidgetState,InitializeHive>(_initializeHive),
]);

InitialSetupWidgetState _verifyPreferences(InitialSetupWidgetState state, VerifyPreferences action) => state;

InitialSetupWidgetState _deletePreferences(InitialSetupWidgetState state, DeletePreferences action) => state;

InitialSetupWidgetState _savePreferences(InitialSetupWidgetState state, SavePreferences action) => state;

InitialSetupWidgetState _initializeHive(InitialSetupWidgetState state, InitializeHive action) => state;


import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:mobile_covid/utils/consts/custom-colors.dart';
import 'package:url_launcher/url_launcher.dart';

class SobrePage extends StatelessWidget {
  static const String tag = 'sobre-page';
  const SobrePage({Key key}) : super(key: key);

  _launchURL() async {
    const url = 'mailto:combatecoronavirus@institutosoma.org.br';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Expanded _sobreAppText() => Expanded(
    child: SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(12),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: AutoSizeText.rich(
                    TextSpan(
                      style: TextStyle(
                        fontSize: 14,
                      ),
                      children: <TextSpan>[
                        TextSpan(text: 'A plataforma tecnológica Combate Coronavírus foi desenvolvida de forma voluntária e disponibilizada sem custo, tendo como missão principal oferecer ferramentas de processamento de dados e inteligência espacial para apoio aos municípios brasileiros no '),
                        TextSpan(text: 'combate à pandemia de COVID-19.', style: TextStyle(fontWeight: FontWeight.bold)),
                      ]
                    )
                  ),
                )
              ],
            ),
            SizedBox(height: 32),
            Row(
              children: <Widget>[
                Expanded(
                  child: AutoSizeText(
                    'Conhece algum município que ainda não faz parte da plataforma?',
                    style: TextStyle(fontSize: 18, color: CustomColors.customDarkGreen, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
            SizedBox(height: 4),
            Text.rich(
              TextSpan(
                style: TextStyle(
                  fontSize: 14,
                ),
                children: <TextSpan>[
                  TextSpan(text: 'Entre em contato com a Prefeitura Municipal e peça para que o responsável envie um e-mail para '),
                  TextSpan(
                    text: 'combatecoronavirus@institutosoma.org.br',
                    style: TextStyle(
                      color: Colors.lightBlue,
                      decoration: TextDecoration.underline,
                    ),
                    recognizer: new TapGestureRecognizer()
                      ..onTap = _launchURL,
                  ),
                  TextSpan(text: ' solicitando acesso.'),
                ],
              ),
            ),
          ],
        ),
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SOBRE O APP'),
      ),
      body: Column(
        children: <Widget>[
          Image.asset(
            'assets/images/title.png',
            height: (MediaQuery.of(context).size.height < 600) ? 100 : 197,
          ),
          _sobreAppText(),
          Image.asset('assets/images/GloboMenor.png'),
        ],
      ),
    );
  }
}
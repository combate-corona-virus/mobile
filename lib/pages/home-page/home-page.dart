import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/keys.dart';
import 'package:mobile_covid/pages/home-page/+model/home-page-model.dart';
import 'package:mobile_covid/pages/home-page/casos-page/casos-page.dart';
import 'package:mobile_covid/pages/home-page/drawer-widget/drawer-widget.dart';
import 'package:mobile_covid/pages/home-page/informacoes-page/informacoes-page.dart';
import 'package:mobile_covid/pages/initial-setup-widget/+state/initial-setup-widget-actions.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/localizacao-municipio-page.dart';
import 'package:mobile_covid/redux/actions.dart';
import 'package:mobile_covid/utils/consts/custom-colors.dart';
import 'package:mobile_covid/utils/create-file.dart';
import 'package:network_to_file_image/network_to_file_image.dart';

class HomePage extends StatefulWidget {
  static const String tag = 'home-page';
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  File logoFile;
  final List<Widget> _widgetOptions = <Widget>[
    CasosPage(),
    InformacoesPage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Padding _topBar(HomePageModel homePageModel) => Padding(
    padding: EdgeInsets.fromLTRB(16, 16, 16, 10),
    child: Row(
      children: <Widget>[
        Expanded(
          child: (homePageModel.municipioSelected['logo'] != null )
          ? Image(
            image: NetworkToFileImage(
              url: homePageModel.municipioSelected['logo']['url'], 
              file: logoFile
            ),
            fit: BoxFit.contain,
            height: 50,
            alignment: Alignment.centerLeft,
          )
          : Container(),
        ),
        InkWell(
          onTap: () => Keys.navKey.currentState.pushReplacementNamed(LocalizacaoMunicipioPage.tag),
          child: Row(
            children: <Widget>[
              Icon(
               Icons.compare_arrows,
               color: CustomColors.customDarkGreen,
              ),
              SizedBox(width: 8),
              AutoSizeText(
                'TROCAR',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: CustomColors.customDarkGreen,
                ),
              )
            ],
          ),
        ),
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, HomePageModel>(
      distinct: true,
      converter: (store) => HomePageModel.fromStore(store),
      onInit: (store) async {
        if(store.state.localizacaoMunicipioPageState.municipioSelected['logo'] != null) {
          logoFile = await file('logo.${store.state.localizacaoMunicipioPageState.municipioSelected['logo']['extensao']}');
        }
      },
      onDispose: (store) {
        store.dispatch(DeletePreferences());
        store.dispatch(new ResetAppState());
      },
      builder: (BuildContext context, HomePageModel homePageModel) {
        return Scaffold(
          appBar: AppBar(
            title: const Text('COMBATE coronavírus'),
          ),
          drawer: DrawerWidget(
            changeTab: _onItemTapped,
            logoFile: logoFile,
            logo: homePageModel.municipioSelected['logo'],
          ),
          body: Column(
            children: <Widget>[
              _topBar(homePageModel),
              Divider(
                thickness: 1,
                height: 1,
              ),
              Expanded(
                child: _widgetOptions.elementAt(_selectedIndex),
              )
            ],
          ),
          bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.insert_chart),
                title: Text('Casos'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.info),
                title: Text('Informações'),
              ),
            ],
            currentIndex: _selectedIndex,
            selectedItemColor: CustomColors.customDarkGreen,
            onTap: _onItemTapped,
          ),
        );
      }
    );
  }
} 
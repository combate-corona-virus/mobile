import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/pages/home-page/+model/home-page-model.dart';
import 'package:mobile_covid/utils/consts/custom-colors.dart';
import 'package:url_launcher/url_launcher.dart';

class InformacoesPage extends StatelessWidget {
  const InformacoesPage({Key key}) : super(key: key);

  /* Padding _howToReportList() => Padding(
    padding: EdgeInsets.fromLTRB(16, 10, 16, 16),
    child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'Neste momento contamos com todos para garantir a saúde, a segurança e o bem-estar de todos',
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'Ouvidoria da Prefeitura de Bauru',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                '(14) 3235-1156',
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'Página oficial da Prefeitura Municipal de Bauru',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: Text.rich(
                TextSpan(
                  style: TextStyle(
                    fontSize: 14,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'https://www2.bauru.sp.gov.br/coronavirus/',
                      style: TextStyle(
                        color: Colors.lightBlue,
                        decoration: TextDecoration.underline,
                      ),
                      recognizer: new TapGestureRecognizer()
                        ..onTap = () => _launchURL('https://www2.bauru.sp.gov.br/coronavirus/'),
                    ),
                  ],
                ),
                textAlign: TextAlign.start,
              ),
            )
          ],
        ),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'Disk-COVID: 192 opção 2',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
      ],
    )
  ); */

  Container _titleGreenBar(BuildContext context) => Container(
    width: MediaQuery.of(context).size.width - 16,
    padding: EdgeInsets.only(left: 16),
    height: 40,
    decoration: BoxDecoration(
      color: CustomColors.customDarkGreen,
    ),
    alignment: Alignment.centerLeft,
    child: Row(
      children: <Widget>[
        Expanded(
          child: AutoSizeText(
            'Contato da Prefeitura',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
        )
      ],
    ),
  );

  Row _checkItem(String text) => Row(
    children: <Widget>[
      Expanded(
        child: AutoSizeText(
          '•  $text',
          maxLines: 4,
          style: TextStyle(
            fontSize: 14,
          ),
        ),
      )
    ],
  );

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Padding _infoList() => Padding(
    padding: EdgeInsets.fromLTRB(16, 10, 16, 16),
    child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'O que fazer no caso de sintomas?',
                maxLines: 1,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: CustomColors.customDarkGreen,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                '• A COVID-19 é uma doença causada pelo coronavírus SARS-CoV-2, que apresenta um quadro clínico que varia de infecções assintomáticas a quadros respiratórios graves. De acordo com a Organização Mundial de Saúde (OMS), a maioria dos pacientes com COVID-19 (cerca de 80%) podem ser assintomáticos e cerca de 20% dos casos podem requerer atendimento hospitalar por apresentarem dificuldade respiratória e desses casos aproximadamente 5% podem necessitar de suporte para o tratamento de insuficiência respiratória (suporte ventilatório).',
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'Quais são os sintomas',
                maxLines: 1,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: CustomColors.customDarkGreen,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'Os sintomas da COVID-19 podem variar de um simples resfriado até uma pneumonia severa. Sendo os sintomas mais comuns:',
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 16),
        _checkItem('Tosse'),
        SizedBox(height: 4),
        _checkItem('Febre'),
        SizedBox(height: 4),
        _checkItem('Coriza'),
        SizedBox(height: 4),
        _checkItem('Dor de garganta'),
        SizedBox(height: 4),
        _checkItem('Dificuldade de respirar'),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'Como é transmitido',
                maxLines: 1,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: CustomColors.customDarkGreen,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'A transmissão acontece de uma pessoa doente para outra ou por contato próximo por meio de:',
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 16),
        _checkItem('Toque do aperto de mão;'),
        SizedBox(height: 4),
        _checkItem('Gotículas de saliva;'),
        SizedBox(height: 4),
        _checkItem('Espirro;'),
        SizedBox(height: 4),
        _checkItem('Tosse;'),
        SizedBox(height: 4),
        _checkItem('Catarro;'),
        SizedBox(height: 4),
        _checkItem('Objetos ou superfícies contaminadas, como celulares, mesas, maçanetas, brinquedos, teclados de computador etc.'),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'Como se proteger',
                maxLines: 1,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: CustomColors.customDarkGreen,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'As recomendações de prevenção à COVID-19 são as seguintes:',
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 16),
        _checkItem('Lave com frequência as mãos até a altura dos punhos, com água e sabão, ou então higienize com álcool em gel 70%.'),
        SizedBox(height: 4),
        _checkItem('Ao tossir ou espirrar, cubra nariz e boca com lenço ou com o braço, e não com as mãos.'),
        SizedBox(height: 4),
        _checkItem('Evite tocar olhos, nariz e boca com as mãos não lavadas.'),
        SizedBox(height: 4),
        _checkItem('Ao tocar, lave sempre as mãos como já indicado.'),
        SizedBox(height: 4),
        _checkItem('Mantenha uma distância mínima de cerca de 2 metros de qualquer pessoa tossindo ou espirrando.'),
        SizedBox(height: 4),
        _checkItem('Evite abraços, beijos e apertos de mãos. Adote um comportamento amigável sem contato físico, mas sempre com um sorriso no rosto.'),
        SizedBox(height: 4),
        _checkItem('Higienize com frequência o celular e os brinquedos das crianças.'),
        SizedBox(height: 4),
        _checkItem('Não compartilhe objetos de uso pessoal, como talheres, toalhas, pratos e copos.'),
        SizedBox(height: 4),
        _checkItem('Mantenha os ambientes limpos e bem ventilados.'),
        SizedBox(height: 4),
        _checkItem('Evite circulação desnecessária nas ruas, estádios, teatros, shoppings, shows, cinemas e igrejas. Se puder, fique em casa.'),
        SizedBox(height: 4),
        _checkItem('Se estiver doente, evite contato físico com outras pessoas, principalmente idosos e doentes crônicos, e fique em casa até melhorar.'),
        SizedBox(height: 4),
        _checkItem('Durma bem e tenha uma alimentação saudável.'),
        SizedBox(height: 4),
        _checkItem('Utilize máscaras caseiras ou artesanais feitas de tecido em situações de saída de sua residência.'),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'Se eu ficar doente',
                maxLines: 1,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: CustomColors.customDarkGreen,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'Caso você se sinta doente, com sintomas de gripe, evite contato físico com outras pessoas, principalmente idosos e doentes crônicos e fique em casa por 14 dias. Só procure um hospital de referência se estiver com falta de ar.',
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'Em caso de diagnóstico positivo para COVID-19, siga as seguintes recomendações:',
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 16),
        _checkItem('Fique em isolamento domiciliar.'),
        SizedBox(height: 4),
        _checkItem('Utilize máscara o tempo todo.'),
        SizedBox(height: 4),
        _checkItem('Se for preciso cozinhar, use máscara de proteção, cobrindo boca e nariz todo o tempo.'),
        SizedBox(height: 4),
        _checkItem('Depois de usar o banheiro, nunca deixe de lavar as mãos com água e sabão e sempre limpe vaso, pia e demais superfícies com álcool ou água sanitária para desinfecção do ambiente.'),
        SizedBox(height: 4),
        _checkItem('Separe toalhas de banho, garfos, facas, colheres, copos e outros objetos apenas para seu uso.'),
        SizedBox(height: 4),
        _checkItem('O lixo produzido precisa ser separado e descartado.'),
        SizedBox(height: 4),
        _checkItem('Sofás e cadeiras também não podem ser compartilhados e precisam ser limpos frequentemente com água sanitária ou álcool 70%.'),
        SizedBox(height: 4),
        _checkItem('Mantenha a janela aberta para circulação de ar do ambiente usado para isolamento e a porta fechada, limpe a maçaneta frequentemente com álcool 70% ou água sanitária.'),
        SizedBox(height: 16),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'Fonte: Ministério da Saúde',
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 2),
        Row(
          children: <Widget>[
            Expanded(
              child: Text.rich(
                TextSpan(
                  style: TextStyle(
                    fontSize: 14,
                  ),
                  children: <TextSpan>[
                    TextSpan(text: 'Para mais informações '),
                    TextSpan(
                      text: 'acesse a página oficial',
                      style: TextStyle(
                        color: Colors.lightBlue,
                        decoration: TextDecoration.underline,
                      ),
                      recognizer: new TapGestureRecognizer()
                        ..onTap = () => _launchURL('https://saude.gov.br/'),
                    ),
                  ],
                ),
                textAlign: TextAlign.start,
              ),
            )
          ],
        ),
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
     return StoreConnector<AppState, HomePageModel>(
      distinct: true,
      converter: (store) => HomePageModel.fromStore(store),
      builder: (BuildContext context, HomePageModel homePageModel) {
        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _infoList(),
              if(homePageModel.municipioSelected['texto_contato'] != null)
                _titleGreenBar(context),
              if(homePageModel.municipioSelected['texto_contato'] != null)
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: AutoSizeText(homePageModel.municipioSelected['texto_contato']),
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      }
    );
  }
} 
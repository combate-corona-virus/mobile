import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/keys.dart';
import 'package:mobile_covid/pages/home-page/fale-conosco-page/+model/fale-conosco-page-model.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/localizacao-municipio-page.dart';
import 'package:mobile_covid/redux/chat/chat-actions.dart';
import 'package:mobile_covid/utils/consts/custom-colors.dart';
import 'package:mobile_covid/utils/create-file.dart';
import 'package:network_to_file_image/network_to_file_image.dart';

class FaleConoscoPage extends StatefulWidget {
  static const String tag = 'fale-conosco-page';
  FaleConoscoPage({Key key}) : super(key: key);

  @override
  _FaleConoscoPageState createState() => _FaleConoscoPageState();
}

class _FaleConoscoPageState extends State<FaleConoscoPage> {
  final TextEditingController _chatController = TextEditingController();
  File logoFile;


  Card _topBar(FaleConoscoPageModel faleConoscoPageModel) => Card(
    elevation: 8,
      child: Padding(
      padding: EdgeInsets.fromLTRB(16, 16, 16, 10),
      child: Row(
        children: <Widget>[
          Expanded(
            child: (faleConoscoPageModel.municipioSelected['logo'] != null )
              ? Image(
                image: NetworkToFileImage(
                  url: faleConoscoPageModel.municipioSelected['logo']['url'], 
                  file: logoFile
                ),
                fit: BoxFit.contain,
                height: 50,
                alignment: Alignment.centerLeft,
              )
              : Container(),
          ),
          InkWell(
            onTap: () => Keys.navKey.currentState.pushReplacementNamed(LocalizacaoMunicipioPage.tag),
            child: Row(
              children: <Widget>[
                Icon(
                 Icons.compare_arrows,
                 color: CustomColors.customDarkGreen,
                ),
                SizedBox(width: 8),
                AutoSizeText(
                  'TROCAR',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: CustomColors.customDarkGreen,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    ),
  );

  _buildCardMessage(Map<String, dynamic> message) => Container(
    padding: message['enviado_por'] == 'MUNICIPE' ? EdgeInsets.fromLTRB(6, 6, 2, 6) : EdgeInsets.fromLTRB(2, 6, 6, 6),
    margin: message['enviado_por'] == 'MUNICIPE' ? EdgeInsets.fromLTRB(0, 0, 2, 0) : EdgeInsets.fromLTRB(2, 0, 0, 0),
    width: double.infinity,
    alignment: message['enviado_por'] == 'MUNICIPE' ? Alignment.centerRight : Alignment.centerLeft,
    child: Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30), bottomLeft: Radius.circular((message['enviado_por'] == 'MUNICIPE') ? 30 : 0), bottomRight: Radius.circular((message['enviado_por'] == 'MUNICIPE') ? 0 : 30)),
        color: message['enviado_por'] == 'MUNICIPE' ? Colors.white :  CustomColors.customLightGreen,
      ),
      child: IntrinsicWidth(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              message['texto'],
              style: (message['enviado_por'] == 'MUNICIPE')
                ? TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(0, 0, 0, 0.7), fontSize: 14)
                : TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 14),
              
            ),
            SizedBox(height: 4),
            Container(
              alignment: Alignment.centerRight,
              child: Text(
                '${formatDate(DateTime.parse(message["created_at"]), [dd, '/', mm, '/', yyyy, ' ', HH, ':', nn, ':', ss])}',
                style: (message['enviado_por'] == 'MUNICIPE')
                  ? TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(0, 0, 0, 0.7), fontSize: 10)
                  : TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 10),
                textAlign: TextAlign.end,
                
              ),
            )
          ],
        ),
      ),
    )
  );

  @override
  Widget build(BuildContext context) {
    return  StoreConnector<AppState, FaleConoscoPageModel>(
      distinct: true,
      converter: (store) => FaleConoscoPageModel.fromStore(store),
      onInit: (store) async {
        if(store.state.chatState.isLoading) {
          store.dispatch(new InitializeChatState());
        } else {
          store.dispatch(new GetAllChatData());
        }
        if(store.state.localizacaoMunicipioPageState.municipioSelected['logo'] != null) {
          logoFile = await file('logo.${store.state.localizacaoMunicipioPageState.municipioSelected['logo']['extensao']}');
        }
      },
      builder: (BuildContext context, FaleConoscoPageModel faleConoscoPageModel) {
        return Scaffold(
          resizeToAvoidBottomPadding: true,
          appBar: AppBar(
            title: FittedBox(
              fit: BoxFit.fitWidth,
              child: Text('FALE COM A PREFEITURA')
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.refresh),
                onPressed: faleConoscoPageModel.getAllChatData
              )
            ],
          ),
          
          body: Container(
          color: Color.fromRGBO(128, 178, 89, 0.15),
            child: (!faleConoscoPageModel.isLoading)
              ? Column(
                  children: <Widget>[
                    _topBar(faleConoscoPageModel),
                    Expanded(
                      child: (faleConoscoPageModel.chatList.isNotEmpty) 
                      ? ListView.builder(
                          reverse: true,
                          itemCount: faleConoscoPageModel.chatList.length,
                          itemBuilder: (BuildContext context, int index) => _buildCardMessage(faleConoscoPageModel.chatList[faleConoscoPageModel.chatList.length - index - 1]),
                        )
                      : Center(child: Text('Não há mensagens!'),)
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            flex: 5,
                            fit: FlexFit.tight,
                            child: Container(
                              child: TextFormField(
                                controller:_chatController,
                                decoration: InputDecoration(
                                  hintText: 'Digite uma mensagem',
                                  alignLabelWithHint: true,
                                  labelStyle: TextStyle(color: Colors.grey, fontSize: 16),
                                  errorStyle: TextStyle(color: Colors.red[200]),
                                  filled: true,
                                  fillColor:  Colors.white,
                                  contentPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0),
                                  border: new OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(30.0)),
                                    borderSide: new BorderSide(
                                      color: Colors.black
                                    ),
                                  ),
                                  focusedBorder: new OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(30.0)),
                                    borderSide: new BorderSide(
                                      color: CustomColors.customDarkGreen
                                    ),
                                  ),
                                  errorBorder: new OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(30.0)),
                                    borderSide: new BorderSide(
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ),
                          SizedBox(width: 4),
                          Flexible(
                            flex: 2,
                            fit: FlexFit.tight,
                            child: ButtonTheme(
                              height: 46,
                              child: RaisedButton(
                                textColor: Colors.white,
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
                                color: CustomColors.customDarkGreen,
                                child: Text('ENVIAR', style: TextStyle(fontSize: 18),),
                                onPressed: (){
                                  if (_chatController.text.isNotEmpty) {
                                    faleConoscoPageModel.sendMessage(_chatController.text);
                                    setState(() {
                                      _chatController.text = '';
                                    });
                                  }
                                }
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                )
                : Center(child: CircularProgressIndicator()),
            )
          );
        }
    );
  }
}
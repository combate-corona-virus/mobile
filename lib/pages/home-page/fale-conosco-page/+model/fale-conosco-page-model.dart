import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/redux/chat/chat-actions.dart';
import 'package:redux/redux.dart';

@immutable
class FaleConoscoPageModel{
  final List<Map<String, String>> chatList;
  final bool isLoading;
  final String deviceId;
  final String deviceName;
  final void Function(String message) sendMessage;
  final void Function() getAllChatData;
  final Map<String, dynamic> municipioSelected;

  FaleConoscoPageModel({
    this.chatList,
    this.isLoading,
    this.deviceId,
    this.deviceName,
    this.sendMessage,
    this.getAllChatData,
    this.municipioSelected,
  });

  @override
  bool operator == (Object other) =>
      identical(this, other) ||
      other is FaleConoscoPageModel &&
          runtimeType == other.runtimeType &&
          ListEquality<Map<String, String>>().equals(chatList, other.chatList) &&
          isLoading == other.isLoading &&
          deviceId == other.deviceId &&
          deviceName == other.deviceName &&
          MapEquality<String, dynamic>().equals(municipioSelected, other.municipioSelected);

  @override
  int get hashCode => 
    chatList.hashCode ^
    isLoading.hashCode ^
    deviceId.hashCode ^
    deviceName.hashCode ^
     municipioSelected.hashCode;


  static FaleConoscoPageModel fromStore(Store<AppState> store) =>
      new FaleConoscoPageModel(
        chatList: store.state.chatState.chatList,
        isLoading: store.state.chatState.isLoading,
        deviceId: store.state.chatState.deviceId,
        deviceName: store.state.chatState.deviceName,
        sendMessage: (String message) => store.dispatch(SendMessage(message)),
        getAllChatData: () => store.dispatch(GetAllChatData()),
        municipioSelected: store.state.localizacaoMunicipioPageState.municipioSelected,
      );
}

import 'package:mobile_covid/pages/home-page/casos-page/+state/casos-page-actions.dart';
import 'package:mobile_covid/pages/home-page/casos-page/+state/casos-page-state.dart';
import 'package:redux/redux.dart';

final casosPageReducers = combineReducers<CasosPageState>([
  TypedReducer<CasosPageState, InitializeCasosPage>(_initializeCasosPage),
  TypedReducer<CasosPageState, InitializeCasosPageSuccess>(_initializeCasosPageSuccess),
  TypedReducer<CasosPageState, UpdateBoletimEpidemiologicoList>(_updateBoletimEpidemiologicoListTime),
  TypedReducer<CasosPageState, UpdateBoletimEpidemiologicoListSuccess>(_updateBoletimEpidemiologicoListTimeSuccess),
  TypedReducer<CasosPageState, UpdateCasosPageIsLoading>(_updateCasosPageIsLoading),
  TypedReducer<CasosPageState, UpdateCasosPageIsLoadingBoletimEpidemiologicoList>(_updateCasosPageIsLoadingBoletimEpidemiologicoList),
]);

CasosPageState _initializeCasosPage(CasosPageState state, InitializeCasosPage action) => state;

CasosPageState _initializeCasosPageSuccess(CasosPageState state, InitializeCasosPageSuccess action) => state.copyWith(
  boletimEpidemiologicoLast: action.boletimEpidemiologicoLast,
  boletimEpidemiologicoList: action.boletimEpidemiologicoList,
  obitosPorIdadeList: action.obitosPorIdadeList,
  graficoTotalizadoresPercentuais: action.graficoTotalizadoresPercentuais,
  isLoading: false,
);

CasosPageState _updateBoletimEpidemiologicoListTime(CasosPageState state, UpdateBoletimEpidemiologicoList action) => state;

CasosPageState _updateBoletimEpidemiologicoListTimeSuccess(CasosPageState state, UpdateBoletimEpidemiologicoListSuccess action) => state.copyWith(
  boletimEpidemiologicoList: action.boletimEpidemiologicoList,
  boletimEpidemiologicoPeriodOfDays: action.boletimEpidemiologicoPeriodOfDays,
  isLoadingBoletimEpidemiologicoList: false
);

CasosPageState _updateCasosPageIsLoading(CasosPageState state, UpdateCasosPageIsLoading action) => state.copyWith(isLoading: action.isLoading);

CasosPageState _updateCasosPageIsLoadingBoletimEpidemiologicoList(CasosPageState state, UpdateCasosPageIsLoadingBoletimEpidemiologicoList action) => state.copyWith(isLoadingBoletimEpidemiologicoList: action.isLoadingBoletimEpidemiologicoList);


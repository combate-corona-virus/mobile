import 'package:meta/meta.dart';

@immutable
class CasosPageState{
  final Map<String, dynamic> boletimEpidemiologicoLast;
  final List<Map<String, dynamic>> boletimEpidemiologicoList;
  final List<Map<String, dynamic>> obitosPorIdadeList;
  final List<Map<String, dynamic>> graficoTotalizadoresPercentuais;
  final int boletimEpidemiologicoPeriodOfDays;
  final bool isLoading;
  final bool isLoadingBoletimEpidemiologicoList;

  CasosPageState({
    @required this.boletimEpidemiologicoLast,
    @required this.boletimEpidemiologicoList,
    @required this.obitosPorIdadeList,
    @required this.graficoTotalizadoresPercentuais,
    @required this.boletimEpidemiologicoPeriodOfDays,
    @required this.isLoading,
    @required this.isLoadingBoletimEpidemiologicoList,
  });

  factory CasosPageState.initial(){
    return new CasosPageState(
      boletimEpidemiologicoLast: {},
      boletimEpidemiologicoList: [],
      obitosPorIdadeList: [],
      graficoTotalizadoresPercentuais: [],
      boletimEpidemiologicoPeriodOfDays: 7,
      isLoading: true,
      isLoadingBoletimEpidemiologicoList: false,
    );
  }

  CasosPageState copyWith({
    Map<String, dynamic> boletimEpidemiologicoLast,
    List<Map<String, dynamic>> boletimEpidemiologicoList,
    List<Map<String, dynamic>> obitosPorIdadeList,
    List<Map<String, dynamic>> graficoTotalizadoresPercentuais,
    int boletimEpidemiologicoPeriodOfDays,
    bool isLoading,
    bool isLoadingBoletimEpidemiologicoList,
  }){
    return new CasosPageState(
      boletimEpidemiologicoLast: boletimEpidemiologicoLast ?? this.boletimEpidemiologicoLast,
      boletimEpidemiologicoList: boletimEpidemiologicoList ?? this.boletimEpidemiologicoList,
      boletimEpidemiologicoPeriodOfDays: boletimEpidemiologicoPeriodOfDays ?? this.boletimEpidemiologicoPeriodOfDays,
      obitosPorIdadeList: obitosPorIdadeList ?? this.obitosPorIdadeList,
      graficoTotalizadoresPercentuais: graficoTotalizadoresPercentuais ?? this.graficoTotalizadoresPercentuais,
      isLoading: isLoading ?? this.isLoading,
      isLoadingBoletimEpidemiologicoList: isLoadingBoletimEpidemiologicoList ?? this.isLoadingBoletimEpidemiologicoList,
    );
  }
}
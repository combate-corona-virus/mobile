class InitializeCasosPage {
  InitializeCasosPage();
}

class InitializeCasosPageSuccess {
  final Map<String, dynamic> boletimEpidemiologicoLast;
  final List<Map<String, dynamic>> boletimEpidemiologicoList;
  final List<Map<String, dynamic>> obitosPorIdadeList;
  final List<Map<String, dynamic>> graficoTotalizadoresPercentuais;
  InitializeCasosPageSuccess(this.boletimEpidemiologicoLast, this.boletimEpidemiologicoList, this.obitosPorIdadeList, this.graficoTotalizadoresPercentuais);
}

class UpdateBoletimEpidemiologicoList {
  final int boletimEpidemiologicoPeriodOfDays;
  UpdateBoletimEpidemiologicoList(this.boletimEpidemiologicoPeriodOfDays);
}

class UpdateBoletimEpidemiologicoListSuccess {
  final int boletimEpidemiologicoPeriodOfDays;
  final List<Map<String, dynamic>> boletimEpidemiologicoList;
  UpdateBoletimEpidemiologicoListSuccess(this.boletimEpidemiologicoPeriodOfDays, this.boletimEpidemiologicoList);
}

class UpdateCasosPageIsLoading {
  final bool isLoading;
  UpdateCasosPageIsLoading(this.isLoading);
}

class UpdateCasosPageIsLoadingBoletimEpidemiologicoList {
  final bool isLoadingBoletimEpidemiologicoList;
  UpdateCasosPageIsLoadingBoletimEpidemiologicoList(this.isLoadingBoletimEpidemiologicoList);
}
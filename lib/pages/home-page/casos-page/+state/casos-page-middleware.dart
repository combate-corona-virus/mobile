import 'package:date_format/date_format.dart';
import 'package:graphql/client.dart';
import 'package:mobile_covid/client/graphql-cliente.dart';
import 'package:mobile_covid/pages/home-page/casos-page/+state/casos-page-actions.dart';
import 'package:redux/redux.dart';

const String getAllBoletimEpidemiologicoQuery = r'''
  query getAllBoletimEpidemiologico($id_municipio: [Int], $data: DateRange) {
    getAllBoletimEpidemiologico(id_municipio: $id_municipio, data: $data, orderBy: [
      {
        field: "data"
        order: ASC
      }
    ]) {
      id
      created_at
      quantidade_suspeitos
      quantidade_confirmados
      quantidade_obitos
      quantidade_descartados
      quantidade_curados
      quantidade_obitos_suspeitos
      data
      municipio {
        nome
      }
    }
  }
''';

const String getPaginatedBoletimEpidemiologicoQuery = r'''
   query getPaginatedBoletimEpidemiologico($first: Int!, $page: Int, $id_municipio: [Int]) {
      getPaginatedBoletimEpidemiologico(first: $first, page: $page, id_municipio: $id_municipio, orderBy: [
        {
          field: "data"
          order: DESC
        }
      ]) {
        paginatorInfo {
          total
        }
        data {
          id
          created_at
          quantidade_suspeitos
          quantidade_confirmados
          quantidade_obitos
          quantidade_descartados
          quantidade_curados
          quantidade_obitos_suspeitos
          data
          municipio {
            nome
          }
        }
      }
    }

''';

const String getGraficoFaixaEtariaQuery = r'''
  query getGraficoFaixaEtaria($id_municipio: Int!) {
    getGraficoFaixaEtaria(id_municipio: $id_municipio) {
      name
      value
    }
  }
''';

const String getGraficoTotalizadoresPercentuaisQuery = r'''
    query getGraficoTotalizadoresPercentuais($id_municipio: Int!) {
      getGraficoTotalizadoresPercentuais(id_municipio: $id_municipio) {
        name
        value
      }
    }
  ''';

class CasosPageMiddleware extends MiddlewareClass {

  @override
  Future call(Store store, action, NextDispatcher next) async {

    if(action is InitializeCasosPage) {
       final WatchQueryOptions _getPaginatedBoletimEpidemiologicoOptions = WatchQueryOptions(
        documentNode: gql(getPaginatedBoletimEpidemiologicoQuery),
        variables: <String, dynamic>{
          "first": 1,
          "page": 1,
          "id_municipio": store.state.localizacaoMunicipioPageState.municipioSelected["id"],
        }
      );
      final _queryGetPaginatedBoletimEpidemiologicoOptionsResult = await BaseGraphQLClient.client.query(_getPaginatedBoletimEpidemiologicoOptions);
      //final sevenDaysFromNow = formatDate(new DateTime.now().subtract(new Duration(days: 7)), [yyyy, '-', mm, '-', dd,]);
      final sevenDaysFromNow = formatDate(new DateTime.utc(2020, 3, 20), [yyyy, '-', mm, '-', dd,]);
      final dateTimeNow = formatDate(new DateTime.now(), [yyyy, '-', mm, '-', dd,]);
      final WatchQueryOptions _getAllBoletimEpidemiologicoOptions = WatchQueryOptions(
        documentNode: gql(getAllBoletimEpidemiologicoQuery),
        variables: <String, dynamic>{
          "id_municipio": store.state.localizacaoMunicipioPageState.municipioSelected["id"],
          "data": {
            "to":  dateTimeNow,
            "from": sevenDaysFromNow,
          }
        }
      );
      final _queryGetAllBoletimEpidemiologicoOptionsResult = await BaseGraphQLClient.client.query(_getAllBoletimEpidemiologicoOptions);
      final WatchQueryOptions getGraficoFaixaEtariaQueryOptions = WatchQueryOptions(
        documentNode: gql(getGraficoFaixaEtariaQuery),
        variables: <String, dynamic>{
          "id_municipio": store.state.localizacaoMunicipioPageState.municipioSelected["id"],
        }
      );
      final getGraficoFaixaEtariaQueryOptionsResult = await BaseGraphQLClient.client.query(getGraficoFaixaEtariaQueryOptions);
      final WatchQueryOptions getGraficoTotalizadoresPercentuaisQueryOptions = WatchQueryOptions(
        documentNode: gql(getGraficoTotalizadoresPercentuaisQuery),
        variables: <String, dynamic>{
          "id_municipio": store.state.localizacaoMunicipioPageState.municipioSelected["id"],
        }
      );
      final getGraficoTotalizadoresPercentuaisQueryOptionsResult = await BaseGraphQLClient.client.query(getGraficoTotalizadoresPercentuaisQueryOptions);
      (_queryGetPaginatedBoletimEpidemiologicoOptionsResult.data["getPaginatedBoletimEpidemiologico"]["data"].length == 0)
        ? store.dispatch(new InitializeCasosPageSuccess({}, [], [], []),)
        : store.dispatch(new InitializeCasosPageSuccess(
          Map<String, dynamic>.from(_queryGetPaginatedBoletimEpidemiologicoOptionsResult.data["getPaginatedBoletimEpidemiologico"]["data"].first),
          List<Map<String, dynamic>>.from(_queryGetAllBoletimEpidemiologicoOptionsResult.data["getAllBoletimEpidemiologico"]),
          List<Map<String, dynamic>>.from(getGraficoFaixaEtariaQueryOptionsResult.data["getGraficoFaixaEtaria"]),
          List<Map<String, dynamic>>.from(getGraficoTotalizadoresPercentuaisQueryOptionsResult.data["getGraficoTotalizadoresPercentuais"]),
        ));
    }

    if(action is UpdateBoletimEpidemiologicoList) {
      final daysFromNow = formatDate(new DateTime.now().subtract(new Duration(days: action.boletimEpidemiologicoPeriodOfDays)), [yyyy, '-', mm, '-', dd,]);
      final dateTimeNow = formatDate(new DateTime.now(), [yyyy, '-', mm, '-', dd,]);
      final WatchQueryOptions _getAllBoletimEpidemiologicoOptions = WatchQueryOptions(
        documentNode: gql(getAllBoletimEpidemiologicoQuery),
        variables: <String, dynamic>{
          "id_municipio": store.state.localizacaoMunicipioPageState.municipioSelected["id"],
          "data": {
            "to":  dateTimeNow,
            "from": daysFromNow,
          }
        }
      );
      final _queryGetAllBoletimEpidemiologicoOptionsResult = await BaseGraphQLClient.client.query(_getAllBoletimEpidemiologicoOptions);
      store.dispatch(new UpdateBoletimEpidemiologicoListSuccess(
        action.boletimEpidemiologicoPeriodOfDays,
        List<Map<String, dynamic>>.from(_queryGetAllBoletimEpidemiologicoOptionsResult.data["getAllBoletimEpidemiologico"])
      ));
    }

     next(action);
  
  }
  
}


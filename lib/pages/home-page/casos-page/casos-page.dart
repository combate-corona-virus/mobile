import 'package:auto_size_text/auto_size_text.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/env.dart';
import 'package:mobile_covid/pages/home-page/casos-page/+model/casos-page-model.dart';
import 'package:mobile_covid/pages/home-page/casos-page/+state/casos-page-actions.dart';
import 'package:mobile_covid/utils/consts/custom-colors.dart';
import 'package:mobile_covid/utils/horizontal-bar-chart/horizontal-bar-chart.dart';
import 'package:mobile_covid/utils/information-modal/information-modal.dart';
import 'package:mobile_covid/utils/time-series-chart/time-series-chart.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:timeago/timeago.dart' as timeago;

class CasosPage extends StatefulWidget {
  static String tag = 'convenios-filtros-page';
  _CasosPageState createState() => _CasosPageState();
}

class _CasosPageState extends State<CasosPage>{

  /* final List<Map<String, dynamic>> _periodOfTimeOptions = [
    {
      "text": "Últimos 7 dias",
      "value": 7
    },
    {
      "text": "Último Mês",
      "value": 30
    },
    {
      "text": "Último Ano",
      "value": 360
    }
  ]; */

  

  Container _informationBox(int infoValue, String infoName, Color boxColor,) => Container(
    width: 107,
    height: 81,
    decoration: BoxDecoration(
      color: boxColor,
      borderRadius: BorderRadius.all(Radius.circular(4)),
    ),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                  '$infoValue',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  shadows: <Shadow>[
                    Shadow(
                      blurRadius: 3.0,
                      color: Color.fromARGB(255, 0, 0, 0),
                    ),
                  ],
                ),
                textAlign: TextAlign.center,
                maxLines: 1,
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                infoName,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  shadows: <Shadow>[
                    Shadow(
                      blurRadius: 2.0,
                      color: Color.fromARGB(255, 0, 0, 0),
                    ),
                  ],
                ),
                maxLines: 2,
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ],
    ),
  );

  Container _indicatorBox(double infoValue, String infoName, Color boxColor) => Container(
    width: 107,
    height: 81,
    decoration: BoxDecoration(
      color: boxColor,
      borderRadius: BorderRadius.all(Radius.circular(4)),
    ),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                (infoName == "Letalidade")
                ? '${infoValue.toStringAsFixed(1).replaceAll('.', ',')}%'
                : '${infoValue.toStringAsFixed(1).replaceAll('.', ',')}',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  shadows: <Shadow>[
                    Shadow(
                      blurRadius: 3.0,
                      color: Color.fromARGB(255, 0, 0, 0),
                    ),
                  ],
                ),
                textAlign: TextAlign.center,
                maxLines: 1,
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                infoName,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  shadows: <Shadow>[
                    Shadow(
                      blurRadius: 2.0,
                      color: Color.fromARGB(255, 0, 0, 0),
                    ),
                  ],
                ),
                maxLines: 2,
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: Icon(Icons.help_outline, color: Colors.white,)
            )
          ],
        ),
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, CasosPageModel>(
      distinct: true,
      converter: (store) => CasosPageModel.fromStore(store),
      onInit: (store) {
        if(store.state.casosPageState.isLoading) {
          store.dispatch(new InitializeCasosPage());
        }
      },
      builder: (BuildContext context, CasosPageModel casosPageModel) {
        return (!casosPageModel.isLoading) 
        ? (casosPageModel.boletimEpidemiologicoList.isNotEmpty)
          ? SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 10, 16, 16),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: AutoSizeText(
                              'Informe epidemiológico',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 4),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: AutoSizeText(
                              'Atualizado em ${formatDate(DateTime.parse(casosPageModel.boletimEpidemiologicoLast["created_at"]), [dd, '/', mm, '/', yyyy, ' ', HH, ':', nn, ':', ss])} (${ timeago.format(DateTime.parse(casosPageModel.boletimEpidemiologicoLast["created_at"]), locale: 'pt_BR')})',
                              style: TextStyle(
                                fontSize: 14,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 16),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          _informationBox((casosPageModel.boletimEpidemiologicoLast["quantidade_confirmados"] != null) ? casosPageModel.boletimEpidemiologicoLast["quantidade_confirmados"] : 0, 'Casos\nconfirmados', Color.fromRGBO(202, 42, 62, 0.8)),
                          _informationBox((casosPageModel.boletimEpidemiologicoLast["quantidade_descartados"] != null) ? casosPageModel.boletimEpidemiologicoLast["quantidade_descartados"] : 0, 'Casos\ndescartados', Color.fromRGBO(128, 178, 89, 1)),
                          _informationBox((casosPageModel.boletimEpidemiologicoLast["quantidade_suspeitos"] != null) ? casosPageModel.boletimEpidemiologicoLast["quantidade_suspeitos"] : 0, 'Aguardando\nresultado', Color.fromRGBO(249, 181, 64, 0.98)),
                        ],
                      ),
                      SizedBox(height: 4),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          _informationBox((casosPageModel.boletimEpidemiologicoLast["quantidade_curados"] != null) ? casosPageModel.boletimEpidemiologicoLast["quantidade_curados"] : 0, 'Casos\ncurados', Color.fromRGBO(82, 151, 190, 1)),
                          _informationBox((casosPageModel.boletimEpidemiologicoLast["quantidade_obitos_suspeitos"] != null) ? casosPageModel.boletimEpidemiologicoLast["quantidade_obitos_suspeitos"] : 0, 'Óbitos\nsuspeitos', Color.fromRGBO(0, 0, 0, 0.7)),
                          _informationBox((casosPageModel.boletimEpidemiologicoLast["quantidade_obitos"] != null) ? casosPageModel.boletimEpidemiologicoLast["quantidade_obitos"] : 0, 'Óbitos\nconfirmados', Colors.black),
                        ],
                      ),
                      SizedBox(height: 16),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: AutoSizeText(
                              'Indicadores',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 4),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: AutoSizeText(
                              'Toque sobre os indicadores para entender seu cálculo.',
                              style: TextStyle(
                                fontSize: 14,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 16),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          InkWell(
                            onTap: () async {
                              await showDialog(
                                context: context,
                                builder: (context) => InformationModalWidget(
                                  title: 'INDICADOR DE INCIDÊNCIA',
                                  indicadorType: 'incidencia',
                                ),
                              );
                            },
                            child: _indicatorBox(casosPageModel.graficoTotalizadoresPercentuais.firstWhere((element) => element['name'] == "Incidência")["value"].toDouble(), 'Incidência', Color.fromRGBO(57, 112, 89, 1))
                          ),
                          InkWell(
                            onTap: () async {
                              await showDialog(
                                context: context,
                                builder: (context) => InformationModalWidget(
                                  title: 'INDICADOR DE MORTALIDADE',
                                  indicadorType: "mortalidade",
                                ),
                              );
                            },
                            child: _indicatorBox(casosPageModel.graficoTotalizadoresPercentuais.firstWhere((element) => element['name'] == "Mortalidade")["value"].toDouble(), 'Mortalidade', Color.fromRGBO(57, 112, 89, 0.85))
                          ),
                          InkWell(
                            onTap: () async {
                              await showDialog(
                                context: context,
                                builder: (context) => InformationModalWidget(
                                  title: 'INDICADOR DE LETALIDADE',
                                  indicadorType: "letalidade",
                                ),
                              );
                            },
                            child: _indicatorBox(casosPageModel.graficoTotalizadoresPercentuais.firstWhere((element) => element['name'] == "Letalidade")["value"].toDouble(), 'Letalidade', Color.fromRGBO(57, 112, 89, 0.7))
                          ),
                        ],
                      ),
                      SizedBox(height: 4),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 16),
                  child: Divider(
                    color: CustomColors.customDarkGreen,
                    thickness: 2,
                    height: 2,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: AutoSizeText(
                              'Evolução do número de casos | Bauru - SP',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 4),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: AutoSizeText(
                              'Atualizado em ${formatDate(DateTime.parse(casosPageModel.boletimEpidemiologicoLast["created_at"]), [dd, '/', mm, '/', yyyy, ' ', HH, ':', nn, ':', ss])} (${ timeago.format(DateTime.parse(casosPageModel.boletimEpidemiologicoLast["created_at"]), locale: 'pt_BR')})',
                              style: TextStyle(
                                fontSize: 14,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 4),
/*                       InkWell(
                        onTap: (){
                          Picker(
                              adapter: PickerDataAdapter<String>(
                                  pickerdata: [_periodOfTimeOptions.map((option) => option['text']).toList()],
                                  isArray: true,
                              ),
                              hideHeader: true,
                              title: Text("Selecione o período de tempo:", style: TextStyle(color: CustomColors.customDarkGreen, fontWeight: FontWeight.bold),),
                              selectedTextStyle: TextStyle(color: CustomColors.customDarkGreen),
                              confirmText: "Confirmar",
                              confirmTextStyle: TextStyle(color: CustomColors.customDarkGreen, fontWeight: FontWeight.bold),
                              cancel: FlatButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text('Cancelar', style: TextStyle(color: CustomColors.customDarkGreen, fontWeight: FontWeight.bold),),
                              ),
                              onConfirm: (Picker picker, List value) {
                                if(_periodOfTimeOptions[value[0]]["value"] != casosPageModel.boletimEpidemiologicoPeriodOfDays) {
                                  casosPageModel.updateCasosPageIsLoadingBoletimEpidemiologicoList(true);
                                  casosPageModel.updateBoletimEpidemiologicoList(_periodOfTimeOptions[value[0]]["value"]);
                                }
                              }
                          ).showDialog(context);
                        },
                        child: Row(
                          children: <Widget>[
                            AutoSizeText(
                              _periodOfTimeOptions.firstWhere((option) => option["value"] == casosPageModel.boletimEpidemiologicoPeriodOfDays)["text"],
                              style: TextStyle(
                                fontSize: 14,
                              ),
                            ),
                            Icon(Icons.arrow_drop_down),
                          ],
                        ),
                      ), */
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.width / 2,
                        child: (!casosPageModel.isLoadingBoletimEpidemiologicoList)
                         ? SimpleTimeSeriesChart.withBoletimEpidemiologicoData(casosPageModel.boletimEpidemiologicoList)
                         : Center(
                           child: CircularProgressIndicator(),
                         )
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 16),
                  child: Divider(
                    color: CustomColors.customDarkGreen,
                    thickness: 2,
                    height: 2,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: AutoSizeText(
                              'Óbitos por faixa etária | Bauru - SP',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 4),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: AutoSizeText(
                              'Atualizado em ${formatDate(DateTime.parse(casosPageModel.boletimEpidemiologicoLast["created_at"]), [dd, '/', mm, '/', yyyy, ' ', HH, ':', nn, ':', ss])} (${ timeago.format(DateTime.parse(casosPageModel.boletimEpidemiologicoLast["created_at"]), locale: 'pt_BR')})',
                              style: TextStyle(
                                fontSize: 14,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 16),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.width,
                        child: (!casosPageModel.isLoadingBoletimEpidemiologicoList)
                         ? HorizontalBarChart(casosPageModel.obitosPorIdadeList)
                         : Center(
                           child: CircularProgressIndicator(),
                         )
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 16),
                  child: Divider(
                    color: CustomColors.customDarkGreen,
                    thickness: 2,
                    height: 2,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: AutoSizeText(
                              'Incidência geográfica dos casos | Bauru - SP',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                              maxLines: 1,
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 4),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: AutoSizeText(
                              'Atualizado em ${formatDate(DateTime.parse(casosPageModel.boletimEpidemiologicoLast["created_at"]), [dd, '/', mm, '/', yyyy, ' ', HH, ':', nn, ':', ss])} (${ timeago.format(DateTime.parse(casosPageModel.boletimEpidemiologicoLast["created_at"]), locale: 'pt_BR')})',
                              style: TextStyle(
                                fontSize: 14,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 4),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: AutoSizeText(
                              'Funções de zoom desativadas por razões de confidencialidade.',
                              style: TextStyle(
                                fontSize: 14,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 16),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.width,
                        child: WebView(
                          initialUrl: '${env.frontUrl}#/mapa-calor/3991',
                          javascriptMode: JavascriptMode.unrestricted,
                          gestureRecognizers: Set()
                            ..add( Factory<VerticalDragGestureRecognizer>(
                              () => VerticalDragGestureRecognizer()..onUpdate = (_) {},
                            )),
                        ),
                      ),
                      SizedBox(height: 16),
                    ],
                  ),
                ),
              ],
            ),
          )
          : Center(
            child: Text('Não há dados!'),
          )
        : Center(
          child: CircularProgressIndicator()
        );
      },
    );
  }
}
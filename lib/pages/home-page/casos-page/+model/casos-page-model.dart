import 'package:collection/collection.dart';
import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/pages/home-page/casos-page/+state/casos-page-actions.dart';
import 'package:redux/redux.dart';

class CasosPageModel{
  final Map<String, dynamic> boletimEpidemiologicoLast;
  final List<Map<String, dynamic>> boletimEpidemiologicoList;
  final List<Map<String, dynamic>> obitosPorIdadeList;
  final List<Map<String, dynamic>> graficoTotalizadoresPercentuais;
  final int boletimEpidemiologicoPeriodOfDays;
  final bool isLoading;
  final bool isLoadingBoletimEpidemiologicoList;
  final Function(int boletimEpidemiologicoPeriodOfDays) updateBoletimEpidemiologicoList;
  final Function(bool isLoading) updateCasosPageIsLoading;
  final Function(bool isLoadingBoletimEpidemiologicoList) updateCasosPageIsLoadingBoletimEpidemiologicoList;

  CasosPageModel({
    this.boletimEpidemiologicoLast,
    this.boletimEpidemiologicoList,
    this.obitosPorIdadeList,
    this.graficoTotalizadoresPercentuais,
    this.boletimEpidemiologicoPeriodOfDays,
    this.isLoading,
    this.isLoadingBoletimEpidemiologicoList,
    this.updateBoletimEpidemiologicoList,
    this.updateCasosPageIsLoading,
    this.updateCasosPageIsLoadingBoletimEpidemiologicoList,
  });

  @override
  bool operator == (Object other) =>
      identical(this, other) ||
      other is CasosPageModel &&
          runtimeType == other.runtimeType &&
          MapEquality<String, dynamic>().equals(boletimEpidemiologicoLast, other.boletimEpidemiologicoLast) &&
          ListEquality<Map<String, dynamic>>().equals(boletimEpidemiologicoList, other.boletimEpidemiologicoList) &&
          ListEquality<Map<String, dynamic>>().equals(obitosPorIdadeList, other.obitosPorIdadeList) &&
          ListEquality<Map<String, dynamic>>().equals(graficoTotalizadoresPercentuais, other.graficoTotalizadoresPercentuais) &&
          boletimEpidemiologicoPeriodOfDays == other.boletimEpidemiologicoPeriodOfDays &&
          isLoading == other.isLoading &&
          isLoadingBoletimEpidemiologicoList == other.isLoadingBoletimEpidemiologicoList;

  @override
  int get hashCode => 
    boletimEpidemiologicoLast.hashCode ^
    boletimEpidemiologicoList.hashCode ^
    obitosPorIdadeList.hashCode ^
    graficoTotalizadoresPercentuais.hashCode ^
    boletimEpidemiologicoPeriodOfDays.hashCode ^
    isLoading.hashCode ^
    isLoadingBoletimEpidemiologicoList.hashCode;


  static CasosPageModel fromStore(Store<AppState> store) =>
      new CasosPageModel(
        boletimEpidemiologicoLast: store.state.casosPageState.boletimEpidemiologicoLast,
        boletimEpidemiologicoList: store.state.casosPageState.boletimEpidemiologicoList,
        obitosPorIdadeList: store.state.casosPageState.obitosPorIdadeList,
        graficoTotalizadoresPercentuais: store.state.casosPageState.graficoTotalizadoresPercentuais,
        boletimEpidemiologicoPeriodOfDays: store.state.casosPageState.boletimEpidemiologicoPeriodOfDays,
        isLoading: store.state.casosPageState.isLoading,
        isLoadingBoletimEpidemiologicoList: store.state.casosPageState.isLoadingBoletimEpidemiologicoList,
        updateBoletimEpidemiologicoList: (int boletimEpidemiologicoPeriodOfDays) => store.dispatch(new UpdateBoletimEpidemiologicoList(boletimEpidemiologicoPeriodOfDays)),
        updateCasosPageIsLoading: (bool isLoading) => store.dispatch(new UpdateCasosPageIsLoading(isLoading)),
        updateCasosPageIsLoadingBoletimEpidemiologicoList: (bool isLoadingBoletimEpidemiologicoList) => store.dispatch(new UpdateCasosPageIsLoadingBoletimEpidemiologicoList(isLoadingBoletimEpidemiologicoList)),
      );
}

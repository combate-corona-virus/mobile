import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:mobile_covid/keys.dart';
import 'package:mobile_covid/pages/home-page/fale-conosco-page/fale-conosco-page.dart';
import 'package:mobile_covid/pages/home-page/sobre-page/sobre-page.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/localizacao-municipio-page.dart';
import 'package:mobile_covid/utils/consts/custom-colors.dart';
import 'package:network_to_file_image/network_to_file_image.dart';

class DrawerWidget extends StatelessWidget {

  const DrawerWidget({
    Key key,
    this.changeTab,
    this.logoFile,
    this.logo,
  }) : super(key: key);

  final Function(int index) changeTab;
  final File logoFile;
  final dynamic logo;

  Container _locationGreenBar(BuildContext context) => Container(
    padding: EdgeInsets.only(left: 16, right: 8),
    height: 36,
    decoration: BoxDecoration(
      color: CustomColors.customDarkGreen,
    ),
    alignment: Alignment.centerLeft,
    child: Row(
      children: <Widget>[
        Expanded(
          child: AutoSizeText(
            'Bauru, SP',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
        ),
        InkWell(
          onTap: () => Keys.navKey.currentState.pushReplacementNamed(LocalizacaoMunicipioPage.tag),
          child: Icon(Icons.compare_arrows, color: Colors.white)
        ),
      ],
    ),
  );

  Column _drawerListTile(IconData icon, String title, [String description]) => Column(
    children: <Widget>[
      Row(
        children: <Widget>[
          Icon(icon),
          SizedBox(width: 8),
          Expanded(
            child: AutoSizeText(
              title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
            ),
          )
        ],
      ),
      if(description != null)
        Row(
          children: <Widget>[
            SizedBox(width: 32),
            Expanded(
              child: AutoSizeText(
                description,
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.grey,
                ),
              ),
            )
          ],
        )
    ],
  );

  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          SizedBox(height: 32),
          Padding(
            padding: EdgeInsets.all(24),
            child: (logo != null )
              ? Image(
                image: NetworkToFileImage(
                  url: logo['url'], 
                  file: logoFile
                ),
                fit: BoxFit.contain,
                height: 100,
                alignment: Alignment.center,
              )
              : Container(),
          ),
          Divider(),
          Image.asset(
            'assets/images/title.png',
          ),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: _locationGreenBar(context),
          ),
          Padding(
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    changeTab(0);
                    Keys.navKey.currentState.pop();
                  },
                  child: _drawerListTile(Icons.insert_chart, 'Casos', 'Acompanhe a contagem e evolução dos casos no município')
                ),
                SizedBox(height: 16),
                InkWell(
                  onTap: () {
                    changeTab(1);
                    Keys.navKey.currentState.pop();
                  },
                  child: _drawerListTile(Icons.info, 'Informaçōes', 'Encontre informações relevantes relacionadas ao seu município ')
                ),
                SizedBox(height: 16),
                InkWell(
                  onTap: () {
                    Keys.navKey.currentState.pushNamed(FaleConoscoPage.tag);
                  },
                  child: _drawerListTile(Icons.message, 'Fale com a Prefeitura', 'Fale com a prefeitura do seu município')
                ),
                SizedBox(height: 16),
                InkWell(
                  onTap: () => Keys.navKey.currentState.pushNamed(SobrePage.tag),
                  child: _drawerListTile(Icons.phone_android, 'Sobre o APP')
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(),
          ),
          Image.asset(
            'assets/images/GloboMenor.png',
          ),
        ],
      ),
    );
  }
}

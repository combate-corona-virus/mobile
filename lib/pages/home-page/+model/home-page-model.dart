
import 'package:flutter/material.dart';
import 'package:mobile_covid/app-state.dart';
import 'package:redux/redux.dart';
import 'package:collection/collection.dart';

@immutable
class HomePageModel{

  final Map<String, dynamic> municipioSelected;

  HomePageModel({ 
    this.municipioSelected
});

  @override
  bool operator == (Object other) =>
      identical(this, other) ||
      other is HomePageModel &&
          runtimeType == other.runtimeType &&
          MapEquality<String, dynamic>().equals(municipioSelected, other.municipioSelected);
  
  @override
  int get hashCode => 
    municipioSelected.hashCode;



  static HomePageModel fromStore(Store<AppState> store) =>
    new HomePageModel(
      municipioSelected: store.state.localizacaoMunicipioPageState.municipioSelected,
    );

}

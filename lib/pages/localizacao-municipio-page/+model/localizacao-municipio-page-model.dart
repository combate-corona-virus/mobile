
import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/pages/initial-setup-widget/+state/initial-setup-widget-actions.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/+state/localizacao-municipio-page-actions.dart';
import 'package:redux/redux.dart';
import 'package:collection/collection.dart';

    
class LocalizacaoMunicipioPageModel{

  final Function() searchForAllMunicipios;
  final Function(String municipioName) searchForMunicipios;
  final Function(Map<String, dynamic> municipioSelected) selectMunicipio;
  final List<Map<String, dynamic>> municipiosList;
  final Map<String, dynamic> municipioSelected;
  final Function(Map<String, dynamic>) savePreferences;

  LocalizacaoMunicipioPageModel({
    this.searchForAllMunicipios,
    this.searchForMunicipios,
    this.selectMunicipio,
    this.municipiosList,
    this.municipioSelected,
    this.savePreferences,
  });

  @override
  bool operator == (Object other) =>
      identical(this, other) ||
      other is LocalizacaoMunicipioPageModel &&
          runtimeType == other.runtimeType &&
          ListEquality<Map<String, dynamic>>().equals(municipiosList, other.municipiosList) &&
          MapEquality<String, dynamic>().equals(municipioSelected, other.municipioSelected);

  @override
  int get hashCode => 
    municipiosList.hashCode ^
    municipioSelected.hashCode;

  static LocalizacaoMunicipioPageModel fromStore(Store<AppState> store) =>
    new LocalizacaoMunicipioPageModel(
      searchForAllMunicipios: () => store.dispatch(new SearchForAllMunicipios()),
      searchForMunicipios: (String municipioName) => store.dispatch(new SearchForMunicipios(municipioName)),
      selectMunicipio: (Map<String, dynamic> municipioSelected) => store.dispatch(new SelectMunicipio(municipioSelected)),
      municipiosList: store.state.localizacaoMunicipioPageState.municipiosList,
      municipioSelected: store.state.localizacaoMunicipioPageState.municipioSelected,
      savePreferences: (Map<String, dynamic> municipioSelected) => store.dispatch(new SavePreferences(municipioSelected)),
    );

}

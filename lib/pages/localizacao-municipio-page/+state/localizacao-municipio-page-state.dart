import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:collection/collection.dart';

@immutable
class LocalizacaoMunicipioPageState{
  final List<Map<String, dynamic>> municipiosList;
  final Map<String, dynamic> municipioSelected;

  LocalizacaoMunicipioPageState({
    @required this.municipiosList,
    @required this.municipioSelected,
  });

  factory LocalizacaoMunicipioPageState.initial(){
    return new LocalizacaoMunicipioPageState(
      municipiosList: [],
      municipioSelected: {},
    );
  }

  @override
  bool operator == (Object other) =>
      identical(this, other) ||
      other is LocalizacaoMunicipioPageState &&
          runtimeType == other.runtimeType &&
          ListEquality<Map<String, dynamic>>().equals(municipiosList, other.municipiosList) &&
          MapEquality<String, dynamic>().equals(municipioSelected, other.municipioSelected);

  @override
  int get hashCode => 
    municipiosList.hashCode ^
    municipioSelected.hashCode;


  LocalizacaoMunicipioPageState copyWith({
    List<Map<String, dynamic>> municipiosList,
    Map<String, dynamic>  municipioSelected,
  }) {
    return new LocalizacaoMunicipioPageState(
      municipiosList:  municipiosList ?? this.municipiosList,
      municipioSelected:  municipioSelected ?? this.municipioSelected,
    );
  }
}

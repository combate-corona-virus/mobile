class SearchForAllMunicipios {
  SearchForAllMunicipios();
}

class SearchForAllMunicipiosSuccess {
  final List<Map<String, dynamic>> municipiosList;
  SearchForAllMunicipiosSuccess(this.municipiosList);
}
class SearchForMunicipios {
  final String municipioName;
  SearchForMunicipios(this.municipioName);
}

class SearchForMunicipiosSuccess {
  final List<Map<String, dynamic>> municipiosList;
  SearchForMunicipiosSuccess(this.municipiosList);
}

class SelectMunicipio {
  final Map<String, dynamic> municipioSelected;
  SelectMunicipio(this.municipioSelected);
}

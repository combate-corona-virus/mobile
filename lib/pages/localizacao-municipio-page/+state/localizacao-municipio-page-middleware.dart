import 'package:graphql/client.dart';
import 'package:mobile_covid/client/graphql-cliente.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/+state/localizacao-municipio-page-actions.dart';
import 'package:redux/redux.dart';

  
const String getAllMunicipioQuery = r'''
  query getAllMunicipio($orderBy: [OrderByClause!]) {
    getAllMunicipio(orderBy: $orderBy) {
      id
      nome
    }
  }
''';

const String getPaginatedMunicipio = r'''
  query getPaginatedMunicipio($nome: String, $first: Int!, $page: Int!) {
    getPaginatedMunicipio(nome: $nome, first: $first, page: $page, , orderBy: [
        {
          field: "populacao"
          order: DESC
        }
      ]) {
      data {
        id
        nome
        populacao
        unidadeFederativa {
          nome
          regiao
        }
        is_participante
        texto_contato
        logo {
          url
          caminho
        }
      }
    }
  }
''';

class LocalizacaoMunicipioPageMiddleware extends MiddlewareClass {
  @override
  Future<void> call(Store store, action, NextDispatcher next) async {

    if (action is SearchForAllMunicipios) {
      final WatchQueryOptions _options = WatchQueryOptions(
        documentNode: gql(getAllMunicipioQuery),
        variables: <String, dynamic>{
          "value": {
            "orderBy": {
              "field": "populacao",
              "order": 1,
            }
          }
        },
      );
      final queryResult = await BaseGraphQLClient.client.query(_options);
      store.dispatch(new SearchForAllMunicipiosSuccess(List<Map<String, dynamic>>.from(queryResult.data['getAllMunicipio'])));
    }

    if (action is SearchForMunicipios) {
      final WatchQueryOptions _options = WatchQueryOptions(
        documentNode: gql(getPaginatedMunicipio),
        variables: <String, dynamic>{
          "page": 1,
          "first": 3,
          "nome": action.municipioName,
          "value": {
            "orderBy": {
              "field": "populacao",
              "order": 1,
            }
          }
        },
      );
      final queryResult = await BaseGraphQLClient.client.query(_options);
      store.dispatch(new SearchForMunicipiosSuccess(List<Map<String, dynamic>>.from(queryResult.data['getPaginatedMunicipio']['data'])));
    }

    next(action);

  }
}

import 'package:mobile_covid/pages/localizacao-municipio-page/+state/localizacao-municipio-page-actions.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/+state/localizacao-municipio-page-state.dart';
import 'package:redux/redux.dart';

final localizacaoMunicipioPageReducers = combineReducers<LocalizacaoMunicipioPageState>([
  TypedReducer<LocalizacaoMunicipioPageState,SearchForAllMunicipios>(_searchForAllMunicipios),
  TypedReducer<LocalizacaoMunicipioPageState,SearchForAllMunicipiosSuccess>(_searchForAllMunicipiosSuccess),
  TypedReducer<LocalizacaoMunicipioPageState,SearchForMunicipios>(_searchForMunicipios),
  TypedReducer<LocalizacaoMunicipioPageState,SearchForMunicipiosSuccess>(_searchForMunicipiosSuccess),
  TypedReducer<LocalizacaoMunicipioPageState,SelectMunicipio>(_selectMunicipio),
]);

LocalizacaoMunicipioPageState _searchForAllMunicipios(LocalizacaoMunicipioPageState state, SearchForAllMunicipios action) => state;

LocalizacaoMunicipioPageState _searchForAllMunicipiosSuccess(LocalizacaoMunicipioPageState state, SearchForAllMunicipiosSuccess action) => state.copyWith(municipiosList: action.municipiosList);

LocalizacaoMunicipioPageState _searchForMunicipios(LocalizacaoMunicipioPageState state, SearchForMunicipios action) => state;

LocalizacaoMunicipioPageState _searchForMunicipiosSuccess(LocalizacaoMunicipioPageState state, SearchForMunicipiosSuccess action) => state.copyWith(municipiosList: action.municipiosList);

LocalizacaoMunicipioPageState _selectMunicipio(LocalizacaoMunicipioPageState state, SelectMunicipio action) => state.copyWith(municipioSelected: action.municipioSelected);
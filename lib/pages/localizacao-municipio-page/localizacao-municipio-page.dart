import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:graphql/client.dart';
import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/client/graphql-cliente.dart';
import 'package:mobile_covid/keys.dart';
import 'package:mobile_covid/pages/cidade-nao-participante-page/cidade-nao-participante-page.dart';
import 'package:mobile_covid/pages/home-page/home-page.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/+model/localizacao-municipio-page-model.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/+state/localizacao-municipio-page-middleware.dart';
import 'package:mobile_covid/utils/default-raised-button/default-raised-button.dart';

class LocalizacaoMunicipioPage extends StatefulWidget {
  static String tag = 'localizacao-municipio-page';
  _LocalizacaoMunicipioPageState createState() => _LocalizacaoMunicipioPageState();
}

class _LocalizacaoMunicipioPageState extends State<LocalizacaoMunicipioPage> {
  final _nomeMunicipioController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  void dispose() {
    _nomeMunicipioController.dispose();
    super.dispose();
  }

  Column _bottomCircle() => Column(
    children: <Widget>[
      Expanded(
        child: Container(
          alignment: AlignmentDirectional.bottomCenter,
          width: MediaQuery.of(context).size.width,
          child: Image.asset('assets/images/GloboMaior.png'),
        ),
      ),
    ],
  );

  InputDecoration _inputDecoration(String hintText, Function(Map<String, dynamic>) selectMunicipio) => InputDecoration(
    hintText: hintText,
    alignLabelWithHint: true,
    labelStyle: TextStyle(color: Colors.grey, fontSize: 16),
    errorStyle: TextStyle(color: Colors.red[200]),
    fillColor:  Color.fromRGBO(131, 179, 93, 1),
    suffixIcon: InkWell(
      child: Icon(Icons.close, color: Colors.grey),
      onTap: () {
        WidgetsBinding.instance.addPostFrameCallback( (_) {
          _nomeMunicipioController.clear();
          selectMunicipio({});
        });
      },
    ),
  );


  Form __formLocalizacaoMunicipioWidget(Function(String municipioName) searchForMunicipio, List<Map<String, dynamic>> municipioList, Function(Map<String, dynamic>) selectMunicipio) => Form(
    key: _formKey,
    child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: AutoSizeText(
                'Localize seu município',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 8),
        TypeAheadFormField(
          hideSuggestionsOnKeyboardHide: false,
          textFieldConfiguration: TextFieldConfiguration(
            controller: this._nomeMunicipioController,
            decoration: _inputDecoration('Digite o nome ou parte do nome.', selectMunicipio),
            //keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            onSubmitted: (value) => SystemChannels.textInput.invokeMethod('TextInput.hide'),
          ),
          noItemsFoundBuilder: (BuildContext context) {
            return Padding(
              padding: EdgeInsets.all(16),
              child: Text('Nenhum município encontrado!'));
          },
          suggestionsCallback: (pattern) async {
            selectMunicipio({});
            final WatchQueryOptions _options = WatchQueryOptions(
              documentNode: gql(getPaginatedMunicipio),
              variables: <String, dynamic>{
                "page": 1,
                "first": 4,
                "nome": pattern,
              },
            );
            final queryResult = await BaseGraphQLClient.client.query(_options);
            return (queryResult.data != null && queryResult.data['getPaginatedMunicipio'] != null)
             ? queryResult.data['getPaginatedMunicipio']['data']
             : [];
          },
          itemBuilder: (context, suggestion) {
            return ListTile(
              title: Text("${suggestion['nome']} - ${suggestion["unidadeFederativa"]["nome"]}"),
            );
          },
          transitionBuilder: (context, suggestionsBox, controller) {
            return suggestionsBox;
          },
          onSuggestionSelected: (suggestion) {
            this._nomeMunicipioController.text = "${suggestion['nome']} - ${suggestion["unidadeFederativa"]["nome"]}";
            selectMunicipio(suggestion);
          },
        ),
      ],
    )
  );

  Row _topArea(double logoImageSize, double spacerHeight, LocalizacaoMunicipioPageModel localizacaoMunicipioPageModel) => Row(
    children: <Widget>[
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: spacerHeight),
            Image.asset(
              'assets/images/title.png',
              height: logoImageSize,
            ),
            SizedBox(height: spacerHeight),
            Padding(
              padding: EdgeInsets.all(24),
              child: __formLocalizacaoMunicipioWidget(
                localizacaoMunicipioPageModel.searchForMunicipios,
                localizacaoMunicipioPageModel.municipiosList,
                localizacaoMunicipioPageModel.selectMunicipio
              ),
            ),
            SizedBox(height: spacerHeight),
            DefaultRaisedButton(
              onPressed: () {
                if(localizacaoMunicipioPageModel.municipioSelected.isNotEmpty)
                  if(!localizacaoMunicipioPageModel.municipioSelected['is_participante']) {
                    Keys.navKey.currentState.pushNamed(
                      CidadeNaoParticipantePage.tag,
                      arguments: localizacaoMunicipioPageModel.municipioSelected['nome'],
                    );
                  } else {
                    localizacaoMunicipioPageModel.savePreferences(localizacaoMunicipioPageModel.municipioSelected);
                    Keys.navKey.currentState.pushReplacementNamed(
                      HomePage.tag,
                    );
                  }
              },
              text: 'Continuar',
              minWidth: 235,
            ),
          ],
        ),
      ),
    ],
  );

  Widget build(BuildContext context) {
    return StoreConnector<AppState, LocalizacaoMunicipioPageModel>(
      distinct: true,
      converter: (store) => LocalizacaoMunicipioPageModel.fromStore(store),
      //onInit: (store) => store.dispatch(new SearchForAllMunicipios()),
      builder: (BuildContext context, LocalizacaoMunicipioPageModel localizacaoMunicipioPageModel) {
        return Scaffold(
          resizeToAvoidBottomPadding: false,
          body: Stack(
            children: <Widget>[
              _bottomCircle(),
              (MediaQuery.of(context).size.height < 600)
              ? _topArea(100, 16, localizacaoMunicipioPageModel)
              : _topArea(197, 32, localizacaoMunicipioPageModel),
            ],
          ),
        );
      }
    ); 
  }

}

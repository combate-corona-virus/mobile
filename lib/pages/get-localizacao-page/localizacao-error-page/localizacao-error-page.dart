/* import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/keys.dart';
import 'package:mobile_covid/pages/cidade-nao-participante-page/cidade-nao-participante-page.dart';
import 'package:mobile_covid/pages/home-page/home-page.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/+model/localizacao-municipio-page-model.dart';
import 'package:mobile_covid/utils/default-raised-button/default-raised-button.dart';

class _LocalizacaoMunicipioPage extends StatelessWidget {

  Column _bottomCircle(BuildContext context) => Column(
    children: <Widget>[
      Expanded(
        child: Container(
          alignment: AlignmentDirectional.bottomCenter,
          width: MediaQuery.of(context).size.width,
          child: Image.asset('assets/images/GloboMaior.png'),
        ),
      ),
    ],
  );



  Row _topArea(double logoImageSize, double spacerHeight, LocalizacaoMunicipioPageModel localizacaoMunicipioPageModel) => Row(
    children: <Widget>[
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: spacerHeight),
            Image.asset(
              'assets/images/title.png',
              height: logoImageSize,
            ),
            SizedBox(height: spacerHeight),
            Padding(
              padding: EdgeInsets.all(24),
              child: __formLocalizacaoMunicipioWidget(
                localizacaoMunicipioPageModel.searchForMunicipios,
                localizacaoMunicipioPageModel.municipiosList,
                localizacaoMunicipioPageModel.selectMunicipio
              ),
            ),
            SizedBox(height: spacerHeight),
            DefaultRaisedButton(
              onPressed: () {
                if(localizacaoMunicipioPageModel.municipioSelected.isNotEmpty)
                  if(localizacaoMunicipioPageModel.municipioSelected['nome'] != 'Bauru') {
                    Keys.navKey.currentState.pushNamed(
                      CidadeNaoParticipantePage.tag,
                      arguments: localizacaoMunicipioPageModel.municipioSelected['nome'],
                    );
                  } else {
                    localizacaoMunicipioPageModel.savePreferences(localizacaoMunicipioPageModel.municipioSelected);
                    Keys.navKey.currentState.pushReplacementNamed(
                      HomePage.tag,
                    );
                  }
              },
              text: 'Continuar',
              minWidth: 235,
            ),
          ],
        ),
      ),
    ],
  );

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          _bottomCircle(context),
          (MediaQuery.of(context).size.height < 600)
          ? _topArea(100, 16)
          : _topArea(197, 32),
        ],
      ),
    );
  }

}
 */
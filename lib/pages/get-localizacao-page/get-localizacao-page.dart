import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:geolocator/geolocator.dart';
import 'package:graphql/client.dart';
import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/client/graphql-cliente.dart';
import 'package:mobile_covid/keys.dart';
import 'package:mobile_covid/pages/cidade-nao-participante-page/cidade-nao-participante-page.dart';
import 'package:mobile_covid/pages/home-page/home-page.dart';
import 'package:mobile_covid/pages/initial-setup-widget/+state/initial-setup-widget-actions.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/+model/localizacao-municipio-page-model.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/+state/localizacao-municipio-page-actions.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/localizacao-municipio-page.dart';

final String getMunicipioByCoordinatesQuery = r'''
  query getMunicipioByCoordinates($coordinates: Coordinates!) {
    getMunicipioByCoordinates(coordinates: $coordinates) {
      id
      nome
      populacao
      unidadeFederativa {
        nome
        regiao
      }
      is_participante
      texto_contato
      logo {
        url
        caminho
      }
    }
  }
''';

class GetLocalizacaoPage extends StatefulWidget {
  static String tag ='get-localizacao-page';
  GetLocalizacaoPage({Key key}) : super(key: key);

  @override
  _GetLocalizacaoPageState createState() => _GetLocalizacaoPageState();
}

class _GetLocalizacaoPageState extends State<GetLocalizacaoPage> {

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, LocalizacaoMunicipioPageModel>(
      distinct: true,
      converter: (store) => LocalizacaoMunicipioPageModel.fromStore(store),
      onInit: (store) async {
        try {
          final Position userPosition = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
          final WatchQueryOptions _getMunicipioByCoordinatesOptions = WatchQueryOptions(
            documentNode: gql(getMunicipioByCoordinatesQuery),
            variables: <String, dynamic>{
              "coordinates": {
                "latitude":  userPosition.latitude,
                "longitude": userPosition.longitude,
              }
            }
          );
          final _getMunicipioByCoordinatesOptionsResult = await BaseGraphQLClient.client.query(_getMunicipioByCoordinatesOptions);
          if(_getMunicipioByCoordinatesOptionsResult.data["getMunicipioByCoordinates"] != null) {
            store.dispatch(new SelectMunicipio(_getMunicipioByCoordinatesOptionsResult.data["getMunicipioByCoordinates"]));
            if(_getMunicipioByCoordinatesOptionsResult.data["getMunicipioByCoordinates"]['nome'] != 'Bauru') {
              Keys.navKey.currentState.pushReplacementNamed(
                CidadeNaoParticipantePage.tag,
                arguments: _getMunicipioByCoordinatesOptionsResult.data["getMunicipioByCoordinates"]['nome'],
              );
            } else {
              store.dispatch(new SavePreferences(_getMunicipioByCoordinatesOptionsResult.data["getMunicipioByCoordinates"]));
              Keys.navKey.currentState.pushReplacementNamed(
                HomePage.tag,
              );
            }
          } else {
            Keys.navKey.currentState.pushReplacementNamed(LocalizacaoMunicipioPage.tag);
          }
        } on Exception {
          Keys.navKey.currentState.pushReplacementNamed(LocalizacaoMunicipioPage.tag);
        }
        
      },
      builder: (BuildContext context, LocalizacaoMunicipioPageModel localizacaoMunicipioPageModel) {
        return Scaffold(
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset("assets/images/logo.png", width: MediaQuery.of(context).size.width * 0.8),
              Container(
                height: 50,
                width: 50,
                child: CircularProgressIndicator()
              ),
              SizedBox(height: 32),
              Row(
                children: <Widget>[
                  Expanded(
                    child: AutoSizeText(
                      "Aguarde enquanto localizamos seu município...",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18
                      ),
                    ),
                  )
                ],
              )
            ],
          )
        );
      }
    ); 
  }
}
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:mobile_covid/keys.dart';
import 'package:mobile_covid/pages/get-localizacao-page/get-localizacao-page.dart';
import 'package:mobile_covid/utils/consts/custom-colors.dart';
import 'package:url_launcher/url_launcher.dart';

class IntroductionPage extends StatefulWidget {
  static String tag = "introduction-page";
  @override
  _IntroductionPageState createState() => _IntroductionPageState();
}

class _IntroductionPageState extends State<IntroductionPage> {
  final introKey = GlobalKey<IntroductionScreenState>();
  int _currentPage = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  dispose() {
    super.dispose();
  }

  _buildBodyInfoPage(String imagePath, String pageNumber, String title, List<TextSpan> text) => Container(
    height: MediaQuery.of(context).size.height * 0.8,
    width: MediaQuery.of(context).size.width,
    child: Column(
      children: <Widget>[
        SizedBox(height: MediaQuery.of(context).size.height * 0.15,),
        Image.asset(imagePath, alignment: Alignment.center,),
        Expanded(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Flexible(
                flex: 1,
                child: AutoSizeText(
                  '$pageNumber.',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 80,
                    color: Colors.white,
                    height: 0.5,
                    fontFamily: "Lemon Milk"
                  ),
                  maxLines: 1,
                  textAlign: TextAlign.left,
                )
              ),
              SizedBox(width: 14),
              Flexible(
                flex: 4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: AutoSizeText(
                            title,
                            style: TextStyle(
                              fontSize: 30,
                              color: Colors.white,
                              fontFamily: "Lemon Milk"
                            ),
                            maxLines: 1,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 4),
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Expanded(
                            child: AutoSizeText.rich(
                              TextSpan(
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                  height: 2
                                ),
                                children: text
                              ),
                              minFontSize: 8,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        )
      ],
    ),
  );

  _buildBodyIntroPage(String imagePath, String title, List<TextSpan> text) => Container(
    height: MediaQuery.of(context).size.height * 0.8,
    width: MediaQuery.of(context).size.width,
    child: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(height: 32),
          Image.asset(imagePath, alignment: Alignment.center,),
          SizedBox(height: 32),
          Row(
            children: <Widget>[
              Expanded(
                child: AutoSizeText(
                  title,
                  style: TextStyle(
                    color: CustomColors.customDarkGreen,
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                  maxLines: 2,
                ),
              )
          ]),
          SizedBox(height: 16),
          Row(
            children: <Widget>[
              Expanded(
                child: AutoSizeText.rich(
                  TextSpan(
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      height: 2
                    ),
                    children: text
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    ),
  );

  PageDecoration pageDecoration(Color color) => PageDecoration(
    descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
    pageColor: color,
  );

  _launchURL() async {
    const url = 'mailto:covid19@institutosoma.org.br';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {    
    return IntroductionScreen(
      key: introKey,
      onChange: (int newPage) => setState(() {
        _currentPage = newPage;
      }),
      pages: [
        PageViewModel(
          titleWidget: Container(),
          //image: ,
          bodyWidget: _buildBodyIntroPage(
            'assets/images/title.png',
            'Como funciona a plataforma?',
            [
              TextSpan(text:'Baseado em '),
              TextSpan(text:'3 pilares', style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(text:' o funcionamento da plataforma Combate Coronavírus compreende a alimentação dos '),
              TextSpan(text:'dados por parte das prefeituras', style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(text:' e órgãos oficiais, o processamento das informações em '),
              TextSpan(text:'ferramentas de inteligência espacial', style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(text:' para uso no gerenciamento e elaboração de estratégias de enfrentamento e sua '),
              TextSpan(text:'comunicação para a população através do app para celular', style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(text:' com informações oficiais e atualizadas sobre a pandemia COVID-19.'),
            ],
          ),
          decoration: pageDecoration(Colors.white),
        ),
        PageViewModel(
          titleWidget: Container(),
          //image: ,
          bodyWidget: _buildBodyInfoPage(
            (Platform.isIOS)
              ? 'assets/images/intro-image-1-ios.png'
              : 'assets/images/intro-image-1.png',
            '1',
            'ALIMENTAÇÃO',
            [
              TextSpan(text:'Prefeituras informam dados básicos', style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(text:' de pacientes, utilizando '),
              TextSpan(text:'padrão do SUS', style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(text:' e suprimindo informações confidenciais'),
            ],
          ),
          decoration: pageDecoration(CustomColors.customDarkGreen),
        ),
        PageViewModel(
          titleWidget: Container(),
          //image: ,
          bodyWidget: _buildBodyInfoPage(
            'assets/images/intro-image-2.png',
            '2',
            'PROCESSAMENTO',
            [
              TextSpan(text:'Gestores públicos', style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(text:' tem acesso a '),
              TextSpan(text:'ferramentas de inteligência espacial,', style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(text:' como análises, gráficos, mapas e projeções'),
            ],
          ),
          decoration: pageDecoration(CustomColors.customLightGreen),
        ),
        PageViewModel(
          titleWidget: Container(),
          //image: ,
          bodyWidget: _buildBodyInfoPage(
            (Platform.isIOS)
              ? 'assets/images/intro-image-3-ios.png'
              : 'assets/images/intro-image-3.png',
            '3',
            'COMUNICAÇÃO',
            [
              TextSpan(text:'Munícipes baixam o app,', style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(text:' e acompanham informações, boletins epidemiológicos, '),
              TextSpan(text:'evolução dos casos', style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(text:' e mapas de calor '),
            ],
          ),
          decoration: pageDecoration(CustomColors.customBlue),
        ),
        PageViewModel(
          titleWidget: Container(),
          //image: ,
          bodyWidget: _buildBodyIntroPage(
            'assets/images/title.png',
            'Como as prefeituras podem participar?',
            [
              TextSpan(text:'Oferecemos uma plataforma web gratuita onde prefeituras municipais podem submeter dados de vigilância epidemiológica suprimindo informações pessoais de pacientes, e a partir destes dados conseguimos gerar indicadores, análises, mapas, gráficos e projeções úteis para a elaboração e suporte de estratégias locais para combate à pandemia. Para participar envie um e-mail para '),
              TextSpan(
              text: 'covid19@institutosoma.org.br',
              style: TextStyle(
                color: Colors.lightBlue,
                decoration: TextDecoration.underline,
              ),
              recognizer: new TapGestureRecognizer()
                ..onTap = _launchURL,
            ),
              TextSpan(text:' utilizando seu e-mail oficial e retornaremos seu acesso à plataforma.'),
            ],
          ),
          decoration: pageDecoration(Colors.white),
        ),
      ],
      onDone: () => Keys.navKey.currentState.pushReplacementNamed(GetLocalizacaoPage.tag),
      showSkipButton: false,
      skipFlex: 0,
      nextFlex: 0,
      next: Text('PRÓXIMO', style: TextStyle(color: (_currentPage == 0) ? Colors.black : Colors.white )),
      done: const Text('CONTINUAR', style: TextStyle(fontWeight: FontWeight.w600)),
      dotsDecorator: DotsDecorator(
        size: Size(10.0, 10.0),
        color: (_currentPage == 0 || _currentPage == 4) ? Color.fromRGBO(49, 112, 88, 0.5) : Color.fromRGBO(255, 255, 255, 0.5),
        activeSize: Size(10.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
        activeColor: (_currentPage == 0 || _currentPage == 4) ? CustomColors.customDarkGreen : Colors.white,
      ),
    );
  }
}

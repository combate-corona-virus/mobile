import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/pages/home-page/casos-page/+state/casos-page-middleware.dart';
import 'package:mobile_covid/pages/initial-setup-widget/+state/initial-setup-widget-middleware.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/+state/localizacao-municipio-page-middleware.dart';
import 'package:mobile_covid/redux/chat/chat-middleware.dart';
import 'package:redux/redux.dart';

final List<Middleware<AppState>> appMiddleware = [
  LocalizacaoMunicipioPageMiddleware(),
  CasosPageMiddleware(),
  InitialSetupWidgetMiddleware(),
  ChatMiddleware(),
];

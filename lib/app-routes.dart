
import 'package:flutter/widgets.dart';
import 'package:mobile_covid/pages/cidade-nao-participante-page/cidade-nao-participante-page.dart';
import 'package:mobile_covid/pages/get-localizacao-page/get-localizacao-page.dart';
import 'package:mobile_covid/pages/home-page/fale-conosco-page/fale-conosco-page.dart';
import 'package:mobile_covid/pages/home-page/home-page.dart';
import 'package:mobile_covid/pages/home-page/sobre-page/sobre-page.dart';
import 'package:mobile_covid/pages/introduction-page/introduction-page.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/localizacao-municipio-page.dart';

final routes = <String, WidgetBuilder> {
  CidadeNaoParticipantePage.tag: (BuildContext context) => CidadeNaoParticipantePage(),
  LocalizacaoMunicipioPage.tag: (BuildContext context) => LocalizacaoMunicipioPage(),
  HomePage.tag: (BuildContext context) => HomePage(),
  SobrePage.tag: (BuildContext context) => SobrePage(),
  IntroductionPage.tag: (BuildContext context) => IntroductionPage(),
  GetLocalizacaoPage.tag: (BuildContext context) => GetLocalizacaoPage(),
  FaleConoscoPage.tag: (BuildContext context) => FaleConoscoPage(),
};

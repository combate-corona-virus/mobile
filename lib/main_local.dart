import 'package:flutter/material.dart';
import 'package:mobile_covid/env.dart';
import 'package:mobile_covid/main.dart';

void main() {
  BuildEnvironment.init(
    flavor: BuildFlavor.local,
    baseUrl: 'http://192.168.0.157/api',
    frontUrl: 'http://localhost:4200/',
    baseUrlGraphql: 'http://192.168.0.157/api/graphql',
    baseWebSocket: 'ws://192.168.0.157/graphql'
  );
  assert(env != null);
  runApp(MyApp());
}
class InitializeChatState {
  InitializeChatState();

}
class InitializeChatStateSuccess {
  final List<Map<String, String>> chatList;
  final String devideId;
  final String devideName;
  InitializeChatStateSuccess(this.chatList, this.devideId, this.devideName);
}

class SendMessage {
  final String message;
  SendMessage(this.message);
}

class SendMessageSuccess {
  final Map<String, String> chatData;
  SendMessageSuccess(this.chatData);
}

class GetAllChatData {
  GetAllChatData();
}

class GetAllChatDataSuccess {
  final List<Map<String, String>> chatList;
  GetAllChatDataSuccess(this.chatList);
}

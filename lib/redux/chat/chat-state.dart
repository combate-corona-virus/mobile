import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:collection/collection.dart';

@immutable
class ChatState{
  final List<Map<String, String>> chatList;
  final bool isLoading;
  final String deviceId;
  final String deviceName;

  ChatState({
    @required this.chatList,
    @required this.isLoading,
    @required this.deviceId,
    @required this.deviceName,
  });

  factory ChatState.initial(){
    return new ChatState(
      chatList: [],
      isLoading: true,
      deviceId: '',
      deviceName: '',
    );
  }

  @override
  bool operator == (Object other) =>
      identical(this, other) ||
      other is ChatState &&
          runtimeType == other.runtimeType &&
          ListEquality<Map<String, String>>().equals(chatList, other.chatList) &&
          isLoading == other.isLoading &&
          deviceId == other.deviceId &&
          deviceName == other.deviceName;

  @override
  int get hashCode => 
    chatList.hashCode ^
    isLoading.hashCode ^
    deviceId.hashCode ^
    deviceName.hashCode;


  ChatState copyWith({
    List<Map<String, String>> chatList,
    bool isLoading,
    String deviceId,
    String deviceName,
  }) {
    return new ChatState(
      chatList:  chatList ?? this.chatList,
      isLoading:  isLoading ?? this.isLoading,
      deviceId:  deviceId ?? this.deviceId,
      deviceName:  deviceName ?? this.deviceName,
    );
  }
}

import 'package:mobile_covid/redux/chat/chat-actions.dart';
import 'package:mobile_covid/redux/chat/chat-state.dart';
import 'package:redux/redux.dart';

final Reducer<ChatState> chatReducers = combineReducers<ChatState>([
  TypedReducer<ChatState, InitializeChatState>(_initializeChatState),
  TypedReducer<ChatState, InitializeChatStateSuccess>(_initializeChatStateSuccess),
  TypedReducer<ChatState, SendMessage>(_sendMessage),
  TypedReducer<ChatState, SendMessageSuccess>(_sendMessageSuccess),
  TypedReducer<ChatState, GetAllChatData>(_getAllChatData),
  TypedReducer<ChatState, GetAllChatDataSuccess>(_getAllChatDataSuccess),
]);

ChatState _initializeChatState(ChatState state, InitializeChatState action) => state;

ChatState _initializeChatStateSuccess(ChatState state, InitializeChatStateSuccess action) => state.copyWith(chatList: action.chatList, deviceId: action.devideId, deviceName: action.devideName, isLoading: false);

ChatState _sendMessage(ChatState state, SendMessage action) => state;

ChatState _sendMessageSuccess(ChatState state, SendMessageSuccess action) {
  final List<Map<String, String>> newChatList = List<Map<String, String>>.from(state.chatList)..add(action.chatData);
  return state.copyWith(chatList: newChatList);
}

ChatState _getAllChatData(ChatState state, GetAllChatData action) => state;

ChatState _getAllChatDataSuccess(ChatState state, GetAllChatDataSuccess action) => state.copyWith(chatList: action.chatList);


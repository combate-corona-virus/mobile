import 'package:graphql/client.dart';
import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/client/graphql-cliente.dart';
import 'package:mobile_covid/redux/chat/chat-actions.dart';
import 'package:mobile_covid/utils/helpers/get-id.dart';
import 'package:redux/redux.dart';

const String getAllChatQuery = r'''
  query getAllChat($filter: ChatFilterInput) {
    getAllChat(filter: $filter) {
      id
      enviado_por
      id_dispositivo
      nome_dispositivo
      texto
      created_at
    }
  }
''';

const String createChatMutation = r'''
  mutation createChat($input: CreateChatInput!) {
    createChat(input: $input) {
      id
      enviado_por
      id_dispositivo
      nome_dispositivo
      texto
      created_at
    }
  }
''';

class ChatMiddleware extends MiddlewareClass {

  @override
  Future call(Store store, action, NextDispatcher next) async {

    if(action is InitializeChatState) {
      final String deviceId = await getId();
      final String deviceName = await getName();
      final WatchQueryOptions _options = WatchQueryOptions(
        documentNode: gql(getAllChatQuery),
        variables: <String, dynamic>{
          "filter": {
            "id_dispositivo": deviceId,
            "id_municipio": (store as Store<AppState>).state.localizacaoMunicipioPageState.municipioSelected['id'],
          }
        },
      );
      final _queryResult = await BaseGraphQLClient.client.query(_options);
      store.dispatch(new InitializeChatStateSuccess(List<Map<String, String>>.generate(_queryResult.data['getAllChat'].length, (int index) => Map<String, String>.from(_queryResult.data['getAllChat'][index])), deviceId, deviceName));

    }

    if(action is SendMessage) {
      final MutationOptions _createChatMutationOptions = MutationOptions(
        documentNode: gql(createChatMutation),
        variables: <String, dynamic>{
          "input": {
            'id_municipio': (store as Store<AppState>).state.localizacaoMunicipioPageState.municipioSelected['id'],
            'id_dispositivo': (store as Store<AppState>).state.chatState.deviceId,
            'nome_dispositivo': (store as Store<AppState>).state.chatState.deviceName,
            'enviado_por': 'MUNICIPE',
            'texto': action.message
          },
        }
      );
      final _createChatMutationResult = await BaseGraphQLClient.client.mutate(_createChatMutationOptions);
      store.dispatch(new SendMessageSuccess(Map<String, String>.from(_createChatMutationResult.data['createChat'])));
    }


    if(action is GetAllChatData) {
      final WatchQueryOptions _options = WatchQueryOptions(
        documentNode: gql(getAllChatQuery),
        variables: <String, dynamic>{
          "filter": {
            "id_dispositivo": (store as Store<AppState>).state.chatState.deviceId,
            "id_municipio": (store as Store<AppState>).state.localizacaoMunicipioPageState.municipioSelected['id'],
          }
        },
      );
      final _queryResult = await BaseGraphQLClient.client.query(_options);
      store.dispatch(new GetAllChatDataSuccess(List<Map<String, String>>.generate(_queryResult.data['getAllChat'].length, (int index) => Map<String, String>.from(_queryResult.data['getAllChat'][index]))));
    }

     next(action);
  
  }
  
}


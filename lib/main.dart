import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:mobile_covid/app-middleware.dart';
import 'package:mobile_covid/app-reducers.dart';
import 'package:mobile_covid/app-routes.dart';
import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/keys.dart';
import 'package:mobile_covid/pages/initial-setup-widget/initial-setup-widget.dart';
import 'package:mobile_covid/utils/consts/custom-colors.dart';
import 'package:redux/redux.dart';
import 'package:timeago/timeago.dart' as timeago;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  
  final store = new Store<AppState>(
    rootReducer,
    initialState: AppState.initial(),
    middleware: appMiddleware,
  );

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    timeago.setLocaleMessages('pt_BR', timeago.PtBrMessages());
    return new StoreProvider<AppState>(
      store: store,
      child: new MaterialApp(
        title: 'Combate Coronavírus',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: 'Roboto',
          primaryColor: CustomColors.customDarkGreen,
        ),
        navigatorKey: Keys.navKey,
        home: InitialSetupWidget(),
        routes: routes,
      ),
    );
  }
}



import 'package:mobile_covid/app-state.dart';
import 'package:mobile_covid/pages/home-page/casos-page/+state/casos-page-reducers.dart';
import 'package:mobile_covid/pages/initial-setup-widget/+state/initial-setup-widget-reducers.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/+state/localizacao-municipio-page-reducers.dart';
import 'package:mobile_covid/redux/actions.dart';
import 'package:mobile_covid/redux/chat/chat-reducers.dart';

AppState appReducers(AppState state, dynamic action) => new AppState(
  localizacaoMunicipioPageState: localizacaoMunicipioPageReducers(state.localizacaoMunicipioPageState, action),
  casosPageState: casosPageReducers(state.casosPageState, action),
  initialSetupWidgetState: initialSetupWidgetReducers(state.initialSetupWidgetState, action),
  chatState: chatReducers(state.chatState, action),
);

AppState rootReducer(AppState state,dynamic action) {
  if(action is ResetAppState)
    return appReducers(AppState.initial(), action);
  return appReducers(state, action);
}

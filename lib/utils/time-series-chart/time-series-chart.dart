/// Timeseries chart example
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class SimpleTimeSeriesChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  SimpleTimeSeriesChart(this.seriesList, {this.animate});


  factory SimpleTimeSeriesChart.withBoletimEpidemiologicoData(List<Map<String, dynamic>> data) {
    return new SimpleTimeSeriesChart(
      _createBoletimEpidemiologicoData(data),
      animate: false,
    );
  }


  @override
  Widget build(BuildContext context) {
    return new charts.TimeSeriesChart(
      seriesList,
      animate: animate,
      dateTimeFactory: const charts.LocalDateTimeFactory(),
      /* primaryMeasureAxis: new charts.NumericAxisSpec(
            tickProviderSpec:
                new charts.BasicNumericTickProviderSpec(desiredTickCount: 8)), */
       behaviors: [
        new charts.SeriesLegend(
          position: charts.BehaviorPosition.bottom,
        )
      ],
    );
  }

   static List<charts.Series<dynamic, DateTime>> _createBoletimEpidemiologicoData(List<Map<String, dynamic>> data) {
    return [
      /* new charts.Series<dynamic, DateTime>(
        id: 'Casos\nSuspeitos',
        colorFn: (_, __) => charts.Color.fromHex(code: '#FED58E'),
        domainFn: (dynamic data, _) => DateTime.parse(data['data']),
        measureFn: (dynamic data, _) => data['quantidade_suspeitos'],
        data: data,
      ), */
      new charts.Series<dynamic, DateTime>(
        id: 'Casos\nConfirmados',
        colorFn: (_, __) => charts.Color.fromHex(code: '#D55565'),
        domainFn: (dynamic data, _) => DateTime.parse(data['data']),
        measureFn: (dynamic data, _) => data['quantidade_confirmados'],
        data: data,
      ),
      new charts.Series<dynamic, DateTime>(
        id: 'Óbitos',
        colorFn: (_, __) => charts.Color.fromHex(code: '#1A1A1A'),
        domainFn: (dynamic data, _) => DateTime.parse(data['data']),
        measureFn: (dynamic data, _) => data['quantidade_obitos'],
        data: data,
      ),
    ];
  }

}

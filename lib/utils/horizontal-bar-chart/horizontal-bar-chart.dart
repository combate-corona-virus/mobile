/// Horizontal bar chart example
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:mobile_covid/utils/colors-charts/colors-charts.dart';

class HorizontalBarChart extends StatelessWidget {
  final List data;

  HorizontalBarChart(this.data);


  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      _createSeriesList(data),
      animate: false,
      vertical: false,
    );
  }

  charts.Color _getChartColor(Color color) {
    return charts.Color(
      r: color.red,
      g: color.green,
      b: color.blue,
      a: color.alpha
    );
  }

 List<charts.Series<dynamic, String>> _createSeriesList(dynamic data) {
    return [
      new charts.Series<dynamic, String>(
        id: 'Sales',
        domainFn: (dynamic data, _) => data['name'],
        measureFn: (dynamic data, _) => data['value'],
        data: data,
        colorFn: (dynamic data, int index) => _getChartColor(colorsCharts[index])
      ),
    ];
  }
}
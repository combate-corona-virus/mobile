import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:mobile_covid/keys.dart';
import 'package:mobile_covid/utils/consts/custom-colors.dart';

class InformationModalWidget extends StatelessWidget {
  const InformationModalWidget({
    Key key,
    @required this.title,
    @required this.indicadorType,
  }) : super(key: key);
  final String title;
  final String indicadorType;
  

  _buildTitle(String text) => Row(
    children: <Widget>[
      Expanded(
        child: AutoSizeText(
          text,
          style: TextStyle(
            fontSize: 18
          ),
        ),
      )
    ],
  );

  /* _buildExemplo(String text) => Row(
    children: <Widget>[
      Expanded(
        child: AutoSizeText(
          text,
          style: TextStyle(
            fontSize: 18,
            color: Color.fromRGBO(0, 0, 0, 0.7),
          ),
        ),
      )
    ],
  ); */

  _buildComparativo(String indicadorType) => Column(
    children: <Widget>[
      Row(
        children: <Widget>[
          Expanded(
            child: AutoSizeText(
              "Comparativo de $indicadorType",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold
              ),
            ),
          )
        ],
      ),
      SizedBox(height: 4),
      Row(
        children: <Widget>[
          Expanded(
            child: AutoSizeText(
              " (atualizado em 11/04/2020 10:56)",
              style: TextStyle(
                fontSize: 12,
              ),
            ),
          )
        ],
      ),
    ],
  );

  _buildCasos(String title, String subTitulo) => Row(
    children: <Widget>[
      Text(
        title,
        style: TextStyle(
          fontSize: 13,
          fontWeight: FontWeight.bold,
        ),
      ),
      Expanded(
        child: AutoSizeText(
          subTitulo,
          style: TextStyle(
            fontSize: 11,
            color: Color.fromRGBO(0, 0, 0, 0.7),
          ),
        ),
      )
    ],
  );
  

@override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(18.0),
      ),
      contentPadding: EdgeInsets.all(0),
      content: Container(
        padding: EdgeInsets.all(16),
        width: MediaQuery.of(context).size.width,
        height: 500,
        child: SingleChildScrollView(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: AutoSizeText(title, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: CustomColors.customDarkGreen), maxLines: 1, textAlign: TextAlign.center,) 
                  ),
                ],
              ),
              SizedBox(height: 24),
              if(indicadorType == "incidencia")
                Column(
                  children: <Widget>[
                    _buildTitle('Número de casos de COVID-19 para cada 100.000 habitantes do município.'),
                    SizedBox(height: 16),
                    _buildComparativo("incidência"),
                    SizedBox(height: 16),
                    _buildCasos("Estado de São Paulo: ", "99,0 casos / 100.000 hab."),
                    SizedBox(height: 16),
                    _buildCasos("Região Sul: ", "26,0 casos / 100.000 hab."),
                    SizedBox(height: 2),
                    _buildCasos("Região Centro-Oeste: ", "28,6 casos / 100.000 hab."),
                    SizedBox(height: 2),
                    _buildCasos("Região Sudeste: ", "79,6 casos / 100.000 hab."),
                    SizedBox(height: 2),
                    _buildCasos("Região Nordeste: ", "92,3 casos / 100.000 hab."),
                    SizedBox(height: 2),
                    _buildCasos("Região Norte: ", "147,6 casos / 100.000 hab."),
                    SizedBox(height: 16),
                    _buildCasos("Brasil: ", "77,4 casos / 100.000 hab."),
                    SizedBox(height: 40),
                    ButtonTheme(
                      minWidth: 150.0,
                      child: RaisedButton(
                        onPressed: () => Keys.navKey.currentState.pop(),
                        color: CustomColors.customDarkGreen,
                        textColor: Colors.white,
                        shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
                        child: Text("FECHAR", style: TextStyle(fontWeight: FontWeight.bold),),
                      ),
                    )
                  ],
                ),
              if(indicadorType == "mortalidade")
                Column(
                  children: <Widget>[
                    _buildTitle('Número de óbitos por COVID-19 para cada 100.000 habitantes do município'),
                    SizedBox(height: 16),
                    _buildComparativo("mortalidade"),
                    SizedBox(height: 16),
                    _buildCasos("Estado de São Paulo: ", "8,1 óbitos / 100.000 hab."),
                    SizedBox(height: 16),
                    _buildCasos("Região Sul: ", "0,9 óbitos / 100.000 hab."),
                    SizedBox(height: 2),
                    _buildCasos("Região Centro-Oeste: ", "0,7 óbitos / 100.000 hab."),
                    SizedBox(height: 2),
                    _buildCasos("Região Sudeste: ", "6,5 óbitos / 100.000 hab."),
                    SizedBox(height: 2),
                    _buildCasos("Região Nordeste: ", "5,5 óbitos / 100.000 hab."),
                    SizedBox(height: 2),
                    _buildCasos("Região Norte: ", "10 óbitos / 100.000 hab."),
                    SizedBox(height: 16),
                    _buildCasos("Brasil: ", "77,4 óbitos / 100.000 hab."),
                    SizedBox(height: 40),
                    ButtonTheme(
                      minWidth: 150.0,
                      child: RaisedButton(
                        onPressed: () => Keys.navKey.currentState.pop(),
                        color: CustomColors.customDarkGreen,
                        textColor: Colors.white,
                        shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
                        child: Text("FECHAR", style: TextStyle(fontWeight: FontWeight.bold),),
                      ),
                    )
                  ],
                ),
              if(indicadorType == "letalidade")
                Column(
                  children: <Widget>[
                    _buildTitle('Percentual de casos de COVID-19 que resultam em óbito no município.'),
                    SizedBox(height: 16),
                    _buildComparativo("letalidade"),
                    SizedBox(height: 16),
                    _buildCasos("Estado de São Paulo: ", "Óbito em 8,16% dos casos"),
                    SizedBox(height: 16),
                    _buildCasos("Região Sul: ", "Óbito em 3,47% dos casos"),
                    SizedBox(height: 2),
                    _buildCasos("Região Centro-Oeste: ", "Óbito em 2,53% dos casos"),
                    SizedBox(height: 2),
                    _buildCasos("Região Sudeste: ", "Óbito em 8,14% dos casos"),
                    SizedBox(height: 2),
                    _buildCasos("Região Nordeste: ", "Óbito em 6,00% dos casos"),
                    SizedBox(height: 2),
                    _buildCasos("Região Norte: ", "Óbito em 6,8% dos casos"),
                    SizedBox(height: 16),
                    _buildCasos("Brasil: ", "Óbito em 8,84% dos casos"),
                    SizedBox(height: 40),
                    ButtonTheme(
                      minWidth: 150.0,
                      child: RaisedButton(
                        onPressed: () => Keys.navKey.currentState.pop(),
                        color: CustomColors.customDarkGreen,
                        textColor: Colors.white,
                        shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
                        child: Text("FECHAR", style: TextStyle(fontWeight: FontWeight.bold),),
                      ),
                    )
                  ],
                ),
            ],
          ),
        ),
      ),  
    );
  }
}

import 'package:flutter/material.dart';
import 'package:mobile_covid/utils/consts/custom-colors.dart';

class DefaultRaisedButton extends StatelessWidget {
  const DefaultRaisedButton({
    Key key,
    @required this.onPressed,
    this.text='Salvar',
    this.fontSize=16,
    this.minWidth=88,
    this.height=36,

  }) : super(key: key);
  final Function onPressed;
  final String text;
  final double fontSize;
  final double minWidth;
  final double height;

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      minWidth: minWidth,
      height: height,
      child: RaisedButton(
        elevation: 8,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(18.0),
        ),
        textColor: Colors.white,
        child: Text(text, style: TextStyle(fontSize: fontSize),),
        onPressed: onPressed,
        color: CustomColors.customDarkGreen,
      ),
    );
  }
}
import 'package:flutter/material.dart';

class CustomColors {

  static const Color customDarkGreen = Color.fromRGBO(49, 112, 88, 1);
  
  static const Color customLightGreen = Color.fromRGBO(128, 178, 90, 1);
  
  static const Color customBlue = Color.fromRGBO(82, 152, 191, 1);

}
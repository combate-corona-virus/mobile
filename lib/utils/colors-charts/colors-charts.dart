import 'package:flutter/material.dart';

final List<Color> colorsCharts = [
  Color.fromRGBO(22, 77, 140, 1),
  Color.fromRGBO(21, 139, 205, 1),
  Color.fromRGBO(236, 140, 141, 1),
  Color.fromRGBO(249, 226, 133, 1),
  Color.fromRGBO(41, 165, 79, 1),
  Color.fromRGBO(255, 194, 137, 1),
  Color.fromRGBO(196, 139, 234, 1),
  Color.fromRGBO(255, 181, 198, 1),
  Color.fromRGBO(160, 217, 149, 1),
  Color.fromRGBO(68, 68, 68, 1),
  Color.fromRGBO(115, 148, 186, 1),
  Color.fromRGBO(115, 185, 225, 1),
  Color.fromRGBO(244, 186, 187, 1),
  Color.fromRGBO(251, 238, 182, 1),
  Color.fromRGBO(127, 201, 149, 1),
  Color.fromRGBO(255, 218, 184, 1),
  Color.fromRGBO(220, 185, 242, 1),
  Color.fromRGBO(255, 211, 221, 1),
  Color.fromRGBO(200, 233, 193, 1),
  Color.fromRGBO(143, 143, 143, 1),
];

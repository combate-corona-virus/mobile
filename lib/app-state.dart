import 'package:meta/meta.dart';
import 'package:mobile_covid/pages/home-page/casos-page/+state/casos-page-state.dart';
import 'package:mobile_covid/pages/initial-setup-widget/+state/initial-setup-widget-state.dart';
import 'package:mobile_covid/pages/localizacao-municipio-page/+state/localizacao-municipio-page-state.dart';
import 'package:mobile_covid/redux/chat/chat-state.dart';

@immutable
class AppState {
  final LocalizacaoMunicipioPageState localizacaoMunicipioPageState;
  final CasosPageState casosPageState;
  final InitialSetupWidgetState initialSetupWidgetState;
  final ChatState chatState;

  AppState({
    @required this.localizacaoMunicipioPageState,
    @required this.casosPageState,
    @required this.initialSetupWidgetState,
    @required this.chatState,
  });

  factory AppState.initial() {
    return AppState(
      localizacaoMunicipioPageState: LocalizacaoMunicipioPageState.initial(),
      casosPageState: CasosPageState.initial(),
      initialSetupWidgetState: InitialSetupWidgetState.initial(),
      chatState: ChatState.initial(),
    );
  }

  AppState copyWith({
    LocalizacaoMunicipioPageState localizacaoMunicipioPageState,
    CasosPageState casosPageState,
    InitialSetupWidgetState initialSetupWidgetState,
    ChatState chatState,
  }
  ) {
    return AppState(
      localizacaoMunicipioPageState: localizacaoMunicipioPageState ?? this.localizacaoMunicipioPageState,
      casosPageState: casosPageState ?? this.casosPageState,
      initialSetupWidgetState: initialSetupWidgetState ?? this.initialSetupWidgetState,
      chatState: chatState ?? this.chatState,
    );
  }
}

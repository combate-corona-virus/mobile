import 'package:graphql/client.dart';
import 'package:mobile_covid/env.dart';

class BaseGraphQLClient {

  static GraphQLClient _client = GraphQLClient(
    cache: InMemoryCache(),
    link: HttpLink(
      uri: env.baseUrlGraphql,
    ),
    defaultPolicies: DefaultPolicies(
      watchQuery: Policies(
        fetch: FetchPolicy.noCache
      ),
      mutate: Policies(
        fetch: FetchPolicy.noCache
      ),
      query: Policies(
        fetch: FetchPolicy.noCache
      ),
    )
  );

  static void setAuthInterceptor(String accessToken) {
    final _authLink = AuthLink(
      getToken: () async => accessToken,
    ).concat(_client.link);
    _client = GraphQLClient(
      cache: InMemoryCache(),
      link: _authLink,
    );
  }

  static GraphQLClient get client => _client; 

}
